# Linear Bandits

- Run on bandits with an increasing number of arms. Currently, the number of arms is fixed at 10

- Fix the horizon 10,000 vs 50,000 problem

# Lipschitz Bandits

- Add (normalized?) regret to the table for both DuSA and OSSB to show dependence regret on the number of arms?

