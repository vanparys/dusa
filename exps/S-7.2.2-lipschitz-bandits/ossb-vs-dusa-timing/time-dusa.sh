#!/bin/bash
#SBATCH --array 1-12
#SBATCH --output Slurm/slurm-%A_%a.out
#SBATCH --cpus-per-task=8
#SBATCH --mem-per-cpu=16000
#SBATCH --time=7-00:00:00
#SBATCH --partition sched_mit_sloan_batch

srun julia dusa-experiment.jl $SLURM_ARRAY_TASK_ID
