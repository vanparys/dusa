#!/bin/bash
#SBATCH --array 1-12
#SBATCH --output Slurm/slurm-%A_%a.out
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=16000
#SBATCH --time=4-00:00:00
#SBATCH --partition sched_mit_sloan_batch

srun julia ossb-experiment.jl $SLURM_ARRAY_TASK_ID
