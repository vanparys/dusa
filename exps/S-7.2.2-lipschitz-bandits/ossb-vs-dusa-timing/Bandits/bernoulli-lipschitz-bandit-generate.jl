import DuSA
import Random
Random.seed!(332)
using JLD
using LinearAlgebra
using Convex
using Mosek

""" 
    lipschitz_bandit(K)

    Construct a L=1/2-Lipschitz Bernouilli bandit instance with K arms 
    embedded in R
"""
function bernoulli_lipschitz_bandit(K)

    N = 2
    R = range(0, 1, length=N)
    
    # Random arms
    X = rand(1, K)
    
    # Construction of reward distribution
    P = zeros(2, K)
    P[2, :] = 0.8 .- 0.5*abs.(0.5.-X)
    P[1, :] = 1 .- P[2, :]
    P = [P[:, x] for x in 1:K]

    L = 0.5
    
    return DuSA.BernoulliLipschitzBandit(P, R, X, L)
end

for K in 5:5:20, instance in 1:3

    println("Generate instance of size K: ", K)

    # Construct a random linear bandit
    B = bernoulli_lipschitz_bandit(K)

    ηstar, μstar = DuSA.deep_target_rates(B)
    save("bernoulli-lipschitz-bandit-size-"*string(K)*"-"*string(instance)*".jld", "B", B, "ηstar", ηstar)

    println("difficulty=", sum(ηstar))

end
