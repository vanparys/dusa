using DataFrames
using CSV
using Statistics
using JLD
using MAT
import DuSA

#################################################

DUSA_RESULTS = DataFrame(size=Integer[], Δₐ=Real[], Δₑ=Real[], Δₙ=Real[], NR=Real[])

for data in filter(x->occursin("DUSA", x), readdir("Data")) 

    println("Data:", data)

    bandit = data[6:maximum(findfirst(".jld", data))]
    ηstar = load("Bandits/"*bandit, "ηstar")
    B = load("Bandits/"*bandit, "B")
    
    D_dusa = CSV.read("Data/"*data, DataFrame)

    ## Analyse
    T = maximum(D_dusa[!, :round])
    K = length(ηstar)
    realized_regret = DuSA.star(B)[2]*T-sum(D_dusa[!, :reward])
    lower_bound = sum([ηstar[x]*DuSA.Δ(x, B)*log(T) for x in 1:K])
    
    R = combine(groupby(D_dusa, :phase), :time => mean => :mean, nrow)
    R = vcat(R, DataFrame(phase="All", mean=mean(D_dusa[!, :time]), nrow=100000))

    Δₐ = 0
    if length(R[findall(s->s=="All", R[!, :phase]), :mean])>0
        Δₐ = R[findall(s->s=="All", R[!, :phase]), :mean][1]
    end
    Δₑ = 0
    if length(R[findall(s->s=="Exploitation", R[!, :phase]), :mean])>0
        Δₑ = R[findall(s->s=="Exploitation", R[!, :phase]), :mean][1]
    end
    Δₙ = 0
    if length(R[findall(s->s=="Exploration/Exploration", R[!, :phase]), :mean])>0
        Δn = R[findall(s->s=="Exploration/Exploration", R[!, :phase]), :mean][1]
    end
    
    
    append!(DUSA_RESULTS,
          DataFrame(
              size=D_dusa[1, :size],
              Δₐ=Δₐ,
              Δₑ=Δₑ,
              Δₙ=Δₙ,
              NR=(realized_regret-lower_bound)/lower_bound
          )) 
    
end

DUSA_RESULTS2 = combine(groupby(DUSA_RESULTS, :size), :Δₐ => mean => :Δₐ, :Δₑ => mean => :Δₑ, :Δₙ => mean => :Δₙ, :NR => mean => :NR, nrow)

CSV.write("Results/DUSA.csv", DUSA_RESULTS2)   


OSSB_RESULTS = DataFrame(size=Integer[], Δₐ=Real[], NR=Real[])

for data in filter(x->occursin("OSSB", x), readdir("Data")) 

    println("Data:", data)

    bandit = data[6:maximum(findfirst(".jld", data))]
    ηstar = load("Bandits/"*bandit, "ηstar")
    println("s=", sum(ηstar))
    B = load("Bandits/"*bandit, "B")
    
    D_ossb = CSV.read("Data/"*data, DataFrame)

    ## Analyse
    T = maximum(D_ossb[!, :round])
    K = length(ηstar)
    realized_regret = DuSA.star(B)[2]*T-sum(D_ossb[!, :reward])
    lower_bound = sum([ηstar[x]*DuSA.Δ(x, B)*log(T) for x in 1:K])
    
    R =
        combine(
            groupby(D_ossb, :phase),
            :time => mean => :mean, nrow
        )
    R = vcat(R, DataFrame(phase="All", mean=mean(D_ossb[!, :time]), nrow=100000))
    
    append!(OSSB_RESULTS,
            DataFrame(
                size=D_ossb[1, :size],
                Δₐ=R[findall(s->s=="All", R[!, :phase]), :mean][1],
                NR=(realized_regret-lower_bound)/lower_bound
            ))

end

OSSB_RESULTS2 = combine(groupby(OSSB_RESULTS, :size), :Δₐ => mean => :Δₐ, :NR => mean => :NR, nrow)

CSV.write("Results/OSSB.csv", OSSB_RESULTS2)


