import DuSA
import Random
using LinearAlgebra
using CSV
using DataFrames
using JLD
using Logging

Random.seed!(32175)

if length(ARGS)==0
    bandit = filter(x->occursin(".jld", x), readdir("Bandits"))[24]
    T = 100
else
    bandit = filter(x->occursin(".jld", x), readdir("Bandits"))[parse(Int, ARGS[1])]
    T = 100000
end

io = open("Logs/"*bandit*".txt", "w+")
IOStream("Logs/"*bandit*".txt")
logger = SimpleLogger(io)
global_logger(logger)

##########
## LOAD ##
##########
B = load("Bandits/"*bandit, "B")
ηstar = load("Bandits/"*bandit, "ηstar")

# Print information
println("Bandit: ", bandit)
println("ηstar=", ηstar)

##########
## OSSB ##
##########
r, x, N, η, s, Bₒ, Δt, ϕ = DuSA.ossb_play(B, T)

##########
## SAVE ##
##########
regret = cumsum([DuSA.Δ(x[t], B) for t in 1:T])
RESULTS = DataFrame(round=1:T, arm=x, size=[length(B.P) for t in 1:T], reward=r, regret=regret, time=Δt, phase=ϕ)
CSV.write("Data/OSSB-"*bandit*".csv", RESULTS)
