#!/bin/bash
#SBATCH --array 1-150%50
#SBATCH --output Slurm/slurm-%A_%a.out
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000
#SBATCH --time=0-12:00:00
#SBATCH --partition sched_mit_sloan_batch

srun julia dusa-play.jl $SLURM_ARRAY_TASK_ID
