using MAT
using JLD
import DuSA
using LinearAlgebra

file = matopen("Lips_150_instances.mat")

K = round(Int, read(file, "num_arms"))
L = read(file, "L")
θs = read(file, "theta")
Xs = read(file, "x")

for i in 1:round(Int, read(file, "num_instance"))

    println("Load and save instance i: ", i)

    P = [[1-θs[i, x], θs[i, x]] for x in 1:K]
    R = range(0, 1, length=2)
    X = hcat(Xs[i, :])'
    
    # Construct a Lipschitz bandit
    B = DuSA.BernoulliLipschitzBandit(P, R, X, L)
    
    ηstar, μstar = DuSA.deep_target_rates(B)

    save("lipschitz-bandit-instance-"*string(i)*".jld", "B", B, "ηstar", ηstar)

end

