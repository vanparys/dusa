#################################################

using DataFrames
using CSV
using Statistics
using JLD
using MAT
using LinearAlgebra
import DuSA

#################################################

# DUSA_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

# for data in filter(x->occursin(".jld", x), readdir("../Data/DUSA"))  

#     bandit = data[1:maximum(findfirst(".jld", data))]

#     println("Bandit: ", bandit)

#     ##################
#     ## DUSA RESULTS ##
#     ##################
#     B = load("../Bandits/"*bandit, "B")
#     ηstar = load("../Bandits/"*bandit, "ηstar")
#     C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

#     R_dusa = CSV.read("../Data/DUSA/"*data, DataFrame)
#     R_dusa[!, :lb] = C*log.(R_dusa[!, :round])
#     R_dusa[!, :realized_regret] = DuSA.star(B)[2].*R_dusa[!, :round]-cumsum(R_dusa[!, :reward])

#     append!(DUSA_RESULTS,
#             DataFrame(
#                 instance=[data for i in 1:size(R_dusa, 1)],
#                 round=R_dusa[!, :round],
#                 expected_regret=R_dusa[!, :regret],
#                 realized_regret=R_dusa[!, :realized_regret],
#                 lower_bound=R_dusa[!, :lb]
#             ))    
# end

# OSSB_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

# for data in filter(x->occursin(".jld", x), readdir("../Data/OSSB")) 

#     bandit = data[6:maximum(findfirst(".jld", data))]

#     println("Bandit: ", bandit)

#     ##################
#     ## OSSB RESULTS ##
#     ##################
#     B = load("../Bandits/"*bandit, "B")
#     ηstar = load("../Bandits/"*bandit, "ηstar")
#     C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

#     R_ossb = CSV.read("../Data/OSSB/"*data, DataFrame)
#     R_ossb[!, :lb] = C*log.(R_ossb[!, :round])
#     R_ossb[!, :realized_regret] = DuSA.star(B)[2].*R_ossb[!, :round]-cumsum(R_ossb[!, :reward])

#     append!(OSSB_RESULTS,
#             DataFrame(
#                 instance=[data for i in 1:size(R_ossb, 1)],
#                 round=R_ossb[!, :round],
#                 expected_regret=R_ossb[!, :regret],
#                 realized_regret=R_ossb[!, :realized_regret],
#                 lower_bound=R_ossb[!, :lb]
#             ))    

# end

CKL_UCB_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

file=matopen("../Data/CKL-UCB/CKL_UCB.mat")
ckl_ucb=read(file, "regret")
close(file)

R_ckl_ucb = DataFrame(round=Integer[], regret=Real[], regret_s2=Real[])
for round in 1:size(ckl_ucb, 2)
    append!(R_ckl_ucb,
            DataFrame(round=round,
                      regret=mean(ckl_ucb[:, round]),
                      regret_s2=sqrt(var(ckl_ucb[:, round]))/sqrt(size(ckl_ucb, 1))
                      ))
end
R_ckl_ucb[!, :regret_min]=R_ckl_ucb[!, :regret].-R_ckl_ucb[!, :regret_s2]
R_ckl_ucb[!, :regret_max]=R_ckl_ucb[!, :regret].+R_ckl_ucb[!, :regret_s2]

CSV.write("aggregate-regret/ckl_ucb.csv", R_ckl_ucb[1:10:end, :])


# ##########
# ## SAVE ##
# ##########

# R_DUSA = DataFrame(round=Integer[], regret=Real[], regret_s2=Real[])
# for subdf in groupby(DUSA_RESULTS, :round)
#     append!(R_DUSA,
#             DataFrame(round=mean(subdf[!, :round]),
#                       regret=mean(subdf[!, :expected_regret]),
#                       regret_s2=sqrt(var(subdf[!, :expected_regret], corrected=false))/sqrt(nrow(subdf)))
#             )
# end

# R_DUSA[!, :regret_min]=R_DUSA[!, :regret].-R_DUSA[!, :regret_s2]
# R_DUSA[!, :regret_max]=R_DUSA[!, :regret].+R_DUSA[!, :regret_s2]

# CSV.write("aggregate-regret/dusa.csv", R_DUSA[1:10:end, :])

# R_OSSB = DataFrame(round=Integer[], regret=Real[], regret_s2=Real[], lower_bound=Real[])
# for subdf in groupby(OSSB_RESULTS, :round)
#     append!(R_OSSB,
#             DataFrame(round=mean(subdf[!, :round]),
#                       regret=mean(subdf[!, :expected_regret]),
#                       regret_s2=sqrt(var(subdf[!, :expected_regret], corrected=false))/sqrt(nrow(subdf)),
#                       lower_bound=mean(subdf[!, :lower_bound]))
#             )
# end

# R_OSSB[!, :regret_min]=R_OSSB[!, :regret].-R_OSSB[!, :regret_s2]
# R_OSSB[!, :regret_max]=R_OSSB[!, :regret].+R_OSSB[!, :regret_s2]

# CSV.write("aggregate-regret/ossb.csv", R_OSSB[1:10:end, :])



