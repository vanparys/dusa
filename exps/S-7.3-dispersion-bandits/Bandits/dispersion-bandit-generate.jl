import DuSA
import Random
Random.seed!(321)
using JLD
using Convex
using Mosek

"""
    dispersion_bandit(K, N)

    Create a generic bandit instance with K arms each of which
    supports N outcomes.
"""
function dispersion_bandit(K, N, γ)

    # Random arms
    R = range(0, 1, length=N)

    # Construction of reward distribution
    P = [rand(N) for x = 1:K]
    P = [P[x]/sum(P[x]) for x =1:K]

    # Distributional Optimization
    Q = Variable(N, K)
    
    constrs =
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [
            sum([R[r]^2*Q[r, x] for r in 1:N]) <= γ[x]*sum([R[r]*Q[r, x] for r in 1:N]) for x in 1:K
        ]
    
    obj = sum([abs(Constant(P[x][r])- Q[r, x]) for x in 1:K, r in 1:N])
    
    problem = minimize(obj, constrs)
    solve!(problem, Mosek.Optimizer(QUIET=true))

    P = [[maximum([Q.value[r, x], 0]) for r in 1:N] for x in 1:K]

    return DuSA.DispersionBandit([P[x]/sum(P[x]) for x in 1:K], R, γ)

end

# Non-Binomial bandits
N = 10

instance = 1
while instance <= 100

    println("Generate instance i: ", instance)

    K = 10
    γ = 0.2*rand(K)+1/N*ones(K)

    # Construct a random linear bandit
    B  = dispersion_bandit(K, N, γ)
    Bg = DuSA.GenericBandit(B.P, B.R)

    ηstar, μstar = DuSA.deep_target_rates(B)
    Cstar = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:K])
    ηg, μg = DuSA.deep_target_rates(Bg)
    Cg = sum([ηg[x]*DuSA.Δ(x, Bg) for x in 1:K])

    if (length(DuSA.star(B)[1]) == 1)
        save("dispersion-bandit-instance-"*string(instance)*".jld", "B", B, "ηstar", ηstar, "Bg", Bg, "ηg", ηg)
        global instance = instance +1
    end

end
