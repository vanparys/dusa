import DuSA
import Random
using JLD
using CSV
using Logging
using DataFrames

# NUMBER OF ROUNDS
if length(ARGS)==0
    bandit = filter(x->occursin(".jld", x), readdir("Bandits"))[1]
    T = 1000
    run = 1
else
    I = findall(s->s==parse(Int, ARGS[1]), reshape(1:400, (100, 4)))[1]
    bandit = filter(x->occursin(".jld", x), readdir("Bandits"))[I[1]]
    T = 10000
    run = I[2]
    Random.seed!(32175+run)
end

io = open("Logs/"*bandit*".txt", "w+")
IOStream("Logs/"*bandit*".txt")
logger = SimpleLogger(io)
global_logger(logger)

B = load("Bandits/"*bandit, "B")
ηstar = load("Bandits/"*bandit, "ηstar")

# Print information
println(B)
println("ηstar=", ηstar)

############
## KL-UCB ##
############
r, x, N, Bₒ, Δt = DuSA.klucb_play(B, T)
regret = cumsum([DuSA.Δ(x[t], B) for t in 1:T])
KL_UCB = DataFrame(round=1:T, arm=x, reward=r, regret=regret, time=Δt)

##########
## DuSA ##
##########
r, x, N, η, μ, s, Bₒ, Δt, ϕ = DuSA.play(B, T)
regret = cumsum([DuSA.Δ(x[t], B) for t in 1:T])
DUSA = DataFrame(round=1:T, arm=x, reward=r, regret=regret, time=Δt, phase=ϕ)

##########
## SAVE ##
##########
CSV.write("Data/kl-ucb-"*bandit*"-run-"*string(run)*".csv", KL_UCB)
CSV.write("Data/dusa-"*bandit*"-run-"*string(run)*".csv", DUSA)

close(io)
