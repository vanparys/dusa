using DataFrames
using CSV
using Statistics
using JLD
using MAT
import DuSA

#################################################

DUSA_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

for data in filter(x->occursin("dusa", x), readdir("Data")) 

    bandit = data[6:maximum(findfirst(".jld", data))]

    println("Bandit: ", bandit)

    ##################
    ## DUSA RESULTS ##
    ##################
    B = load("Bandits/"*bandit, "B")
    ηstar = load("Bandits/"*bandit, "ηstar")
    C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

    R_dusa = CSV.read("Data/"*data, DataFrame)
    R_dusa[:lb] = C*log.(R_dusa[:round])
    R_dusa[:realized_regret] = DuSA.star(B)[2].*R_dusa[!, :round]-cumsum(R_dusa[!, :reward])

    append!(DUSA_RESULTS,
            DataFrame(
                instance=[data for i in 1:size(R_dusa, 1)],
                round=R_dusa[:round],
                expected_regret=R_dusa[:regret],
                realized_regret=R_dusa[:realized_regret],
                lower_bound=R_dusa[:lb]
            ))    
end

KLUCB_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

for data in filter(x->occursin("kl-ucb", x), readdir("Data")) 

    bandit = data[8:maximum(findfirst(".jld", data))]

    println("Bandit: ", bandit)

    ##################
    ## KL-UCB RESULTS ##
    ##################
    B = load("Bandits/"*bandit, "B")
    ηstar = load("Bandits/"*bandit, "ηstar")
    C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

    R_kl_ucb = CSV.read("Data/"*data, DataFrame)
    R_kl_ucb[:lb] = C*log.(R_kl_ucb[:round])
    R_kl_ucb[:realized_regret] = DuSA.star(B)[2].*R_kl_ucb[!, :round]-cumsum(R_kl_ucb[!, :reward])

    append!(KLUCB_RESULTS,
            DataFrame(
                instance=[data for i in 1:size(R_kl_ucb, 1)],
                round=R_kl_ucb[:round],
                expected_regret=R_kl_ucb[:regret],
                realized_regret=R_kl_ucb[:realized_regret],
                lower_bound=R_kl_ucb[:lb]
            ))    

end

##########
## SAVE ##
##########

rows = length(groupby(DUSA_RESULTS, :instance))
dusa_expected_regret=zeros(rows, 10000)
dusa_realized_regret=zeros(rows, 10000)
dusa_lower_bound=zeros(rows, 10000)
subdfs = groupby(DUSA_RESULTS, :instance)

for i in 1:rows
    subdf=subdfs[i]
    dusa_expected_regret[i, :] = subdf[:expected_regret]
    dusa_realized_regret[i, :] = subdf[:realized_regret]    
    dusa_lower_bound[i, :] = subdf[:lower_bound]
end

rows = length(groupby(KLUCB_RESULTS, :instance))
kl_ucb_expected_regret=zeros(rows, 10000)
kl_ucb_realized_regret=zeros(rows, 10000)
kl_ucb_lower_bound=zeros(rows, 10000)
subdfs = groupby(KLUCB_RESULTS, :instance)

for i in 1:rows
    subdf=subdfs[i]
    kl_ucb_expected_regret[i, :] = subdf[:expected_regret]
    kl_ucb_realized_regret[i, :] = subdf[:realized_regret]
    kl_ucb_lower_bound[i, :] = subdf[:lower_bound]
end

matwrite("Results/DISPERSION-DUSA-RESULTS.mat",
             Dict(
	         "Instance" => DUSA_RESULTS[!, :instance],
                 "Expected_Regret" => dusa_expected_regret,
                 "Realized_Regret" => dusa_realized_regret,
                 "LB" => dusa_lower_bound
             )
         )

matwrite("Results/DISPERSION-KLUCB-RESULTS.mat",
             Dict(
	         "Instance" => KLUCB_RESULTS[!, :instance],
                 "Expected_Regret" => kl_ucb_expected_regret,
                 "Realized_Regret" => kl_ucb_realized_regret,
                 "LB" => kl_ucb_lower_bound
             )
         )

