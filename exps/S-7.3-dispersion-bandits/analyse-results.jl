#################################################

using DataFrames
using CSV
using Statistics
using JLD
using MAT
import DuSA

#################################################

DUSA_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

for data in filter(x->occursin("dusa", x), readdir("Data")) 

    bandit = data[6:maximum(findfirst(".jld", data))]

    println("Bandit: ", bandit)

    ##################
    ## DUSA RESULTS ##
    ##################
    B = load("Bandits/"*bandit, "B")
    ηstar = load("Bandits/"*bandit, "ηstar")
    C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

    R_dusa = CSV.read("Data/"*data, DataFrame)
    R_dusa[:lb] = C*log.(R_dusa[:round])
    R_dusa[:realized_regret] = DuSA.star(B)[2].*R_dusa[!, :round]-cumsum(R_dusa[!, :reward])

    append!(DUSA_RESULTS,
            DataFrame(
                instance=[data for i in 1:size(R_dusa, 1)],
                round=R_dusa[:round],
                expected_regret=R_dusa[:regret],
                realized_regret=R_dusa[:realized_regret],
                lower_bound=R_dusa[:lb]
            ))    
end

KLUCB_RESULTS = DataFrame(instance=String[], round=Integer[], realized_regret=Real[], expected_regret=Real[], lower_bound=Real[])

for data in filter(x->occursin("kl-ucb", x), readdir("Data")) 

    bandit = data[8:maximum(findfirst(".jld", data))]

    println("Bandit: ", bandit)

    ##################
    ## KL-UCB RESULTS ##
    ##################
    B = load("Bandits/"*bandit, "B")
    ηstar = load("Bandits/"*bandit, "ηstar")
    C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

    R_kl_ucb = CSV.read("Data/"*data, DataFrame)
    R_kl_ucb[:lb] = C*log.(R_kl_ucb[:round])
    R_kl_ucb[:realized_regret] = DuSA.star(B)[2].*R_kl_ucb[!, :round]-cumsum(R_kl_ucb[!, :reward])

    append!(KLUCB_RESULTS,
            DataFrame(
                instance=[data for i in 1:size(R_kl_ucb, 1)],
                round=R_kl_ucb[:round],
                expected_regret=R_kl_ucb[:regret],
                realized_regret=R_kl_ucb[:realized_regret],
                lower_bound=R_kl_ucb[:lb]
            ))    

end


# ##########
# ## SAVE ##
# ##########

R_DUSA = DataFrame(round=Integer[], regret=Real[], regret_s2=Real[])
for subdf in groupby(DUSA_RESULTS, :round)
    append!(R_DUSA,
            DataFrame(round=mean(subdf[!, :round]),
                      regret=mean(subdf[!, :expected_regret]),
                      regret_s2=sqrt(var(subdf[!, :expected_regret], corrected=false))/sqrt(nrow(subdf)))
            )
end

R_KLUCB = DataFrame(round=Integer[], regret=Real[], regret_s2=Real[])
for subdf in groupby(KLUCB_RESULTS, :round)
    append!(R_KLUCB,
            DataFrame(round=mean(subdf[!, :round]),
                      regret=mean(subdf[!, :expected_regret]),
                      regret_s2=sqrt(var(subdf[!, :expected_regret], corrected=false))/sqrt(nrow(subdf)))
            )
end


##########
## SAVE ##
##########

R = DataFrame(round=R_DUSA[:round],
              dusa_regret=R_DUSA[:regret],
              dusa_regret_s2=R_DUSA[:regret_s2],
              kl_ucb_regret=R_KLUCB[:regret],
              kl_ucb_regret_s2=R_KLUCB[:regret_s2])

R[:dusa_regret_min]=R[:dusa_regret].-R[:dusa_regret_s2]
R[:dusa_regret_max]=R[:dusa_regret].+R[:dusa_regret_s2]
R[:kl_ucb_regret_min]=R[:kl_ucb_regret].-R[:kl_ucb_regret_s2]
R[:kl_ucb_regret_max]=R[:kl_ucb_regret].+R[:kl_ucb_regret_s2]

##########
## NONDECEITFUL
##########

bandit = "dispersion-bandit-instance-58.jld"
B = load("Bandits/"*bandit, "B")
ηstar = load("Bandits/"*bandit, "ηstar")
C = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:length(ηstar)])

R_dusa_nd = vcat(map(s->CSV.read(s, DataFrame), "Data/".*filter(x->occursin(bandit, x) && occursin("dusa", x), readdir("Data")))...)
R[:dusa_nd]=combine(groupby(R_dusa_nd, :round), :regret => mean => :regret)[!, :regret]

R_kl_ucb_nd = vcat(map(s->CSV.read(s, DataFrame), "Data/".*filter(x->occursin(bandit, x) && occursin("kl-ucb", x), readdir("Data")))...)
R[:kl_ucb_nd]=combine(groupby(R_kl_ucb_nd, :round), :regret => mean => :regret)[!, :regret]

CSV.write("Results/Results.csv", R[1:10:end, :])
