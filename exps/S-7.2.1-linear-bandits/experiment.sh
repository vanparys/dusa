#!/bin/bash

#SBATCH --job-name=lindusa
#SBATCH --ntasks=21
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=0-4:00:00
#SBATCH --partition sched_mit_sloan_batch
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --mail-user=bart@vanparys.ch

# Call your script as you would from the command line
julia experiment.jl
