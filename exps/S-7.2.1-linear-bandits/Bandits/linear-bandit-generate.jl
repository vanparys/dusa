import DuSA
import Random
Random.seed!(321)
using JLD
using Convex
using Mosek
using MAT

"""
    discrete_normal(R, μ)

    Returns an random distribution with mean μ supported on R

    Solves the problem

        min I(P, Q)
         st ∑ Q(r) = 1
            ∑ r Q(r) = μ

    with P a random distribution on the simplex
"""
function distribution_with_mean(R, μ)
    N = length(R)

    P = rand(N)
    P = P/sum(P)
    
    # Distributional optimization
    Q = Variable(N)
    constrs = [Q>=0, sum(Q)==1, sum([Q[r]*R[r] for r in 1:N]) == μ]
    problem = minimize(relative_entropy(Constant(P), Q), constrs)

    solve!(problem, Mosek.Optimizer; silent_solver=true)

    return Q.value[:]
end

""" 
    linear_bandit(K, d, N)

    Construct a linear bandit instance with K arms embedded in R^d+1
"""
function linear_bandit(K, d, N)

    # Random arms
    X = randn(d, K)
    X = X./repeat([norm(X[:, k], 2) for k =1:K], 1, d)'

    # Random unknown linear function
    a = randn(d)
    μ = (a'X)[:]

    offset = 0.1    
    
    L = (maximum(μ)-minimum(μ))*1/(1-2*offset)
    Xp = vcat(X, ones(K)')/L
    ap = vcat(a, -minimum(μ)+L*offset)
    μp = (ap'Xp)[:]
    
    # Set R construction
    R = range(0, 1, length=N)

    # Construction of reward distribution
    P = [zeros(N) for x = 1:K]
    Threads.@threads for x in 1:K
        P[x] = distribution_with_mean(R, μp[x])
    end

    return DuSA.LinearBandit(P, R, Xp)
end


# Binomial bandit
d = 3
N = 2

Ks = repeat([5, 10, 15, 20], 120)

for i in 1:size(Ks,1)

    println("Generate instance i: ", i)

    # Construct a random linear bandit
    B = linear_bandit(Ks[i], d, N)
    ηstar, μstar = DuSA.deep_target_rates(B)
    # linear coefficients
    μ = [DuSA.rew(x, B) for x in 1:length(B.P)]
    θ = B.X'\μ
    
    save(string(Ks[i])*"/linear-bandit-instance-"*string(i)*".jld",
         "P", B.P,
         "X", B.X,
         "R", B.R,
         "ηstar", ηstar
         )

    # linear coefficients
    μ = [DuSA.rew(x, B) for x in 1:length(B.P)]
    θ = B.X'\μ

    matwrite(string(Ks[i])*"/linear-bandit-instance-"*string(i)*".mat",
             Dict(
	         "P" => vcat(B.P'...),
	         "X" => B.X,
                 "R" => vcat(B.R),
                 "eta" => ηstar,
                 "theta" => θ
             )
             )

end
