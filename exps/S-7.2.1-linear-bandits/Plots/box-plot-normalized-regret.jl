using DataFrames
using CSV
using Statistics
using JLD
using MAT
import DuSA
using LinearAlgebra

#################################################

RESULTS_DUSA = DataFrame(instance=String[], round=Integer[], expected_regret=Real[], realized_regret=Real[], normalized_regret=Real[], lower_bound=Real[])

for data in readdir("../Data")

    bandit = data[1:maximum(findfirst(".mat", data))]

    # Load Bandit
    file=matopen(joinpath("../Bandits", bandit))
    P = read(file, "P")
    P=[P[x, :] for x in 1:size(P,1)]
    R = read(file, "R")
    X = read(file, "X");    
    ηstar = read(file, "eta")
    close(file)

    B = DuSA.LinearBandit(P, R, X)

    R = CSV.read("../Data/"*data, DataFrame)
    
    ## Analyse
    T = maximum(R[!, :round])
    K = length(ηstar)
    R[!,:realized_regret] = cumsum(DuSA.star(B)[2]*ones(T)).-cumsum(R[!, :reward])
    R[!,:lower_bound] = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:K]).*[log(t) for t in 1:T]
    R[!,:normalized_regret] = R[!,:realized_regret]./R[!,:lower_bound]
    
    R=R[1000:1000:10000, :]
    
    append!(RESULTS_DUSA, DataFrame(instance=bandit, round = R[!, :round], expected_regret=R[!, :regret], realized_regret=R[!,:realized_regret], normalized_regret=R[!, :normalized_regret], lower_bound=R[!,:lower_bound]))
    
end

for subdf in groupby(RESULTS_DUSA, :round)
    round = subdf[1, :round][1]
    CSV.write("box-plot-normalized-regret/DUSA-"*string(round)*".csv", subdf)
end


# #################################################

# RESULTS_OSSB = DataFrame(instance=String[], round=Integer[], expected_regret=Real[], realized_regret=Real[], normalized_regret=Real[], lower_bound=Real[])

# for data in readdir("OSSB")

#     bandit = data[6:maximum(findfirst(".jld", data))]
    
#     println("Bandit: ", bandit)
#     ηstar = load("Bandits/"*bandit, "ηstar")
#     B = load("Bandits/"*bandit, "B")

#     R = CSV.read("OSSB/"*data, DataFrame)

#     ## Analyse
#     T = maximum(R[!, :round])
#     K = length(ηstar)
#     R[!,:realized_regret] = cumsum(DuSA.star(B)[2]*ones(T)).-cumsum(R[!, :reward])
#     R[!,:lower_bound] = sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:K]).*[log(t) for t in 1:T]
#     R[!,:normalized_regret] = R[!,:realized_regret]./R[!,:lower_bound]
    
#     R=R[1000:1000:10000, :]

#     append!(RESULTS_OSSB, DataFrame(instance=bandit, round = R[!, :round], expected_regret=R[!, :regret], realized_regret=R[!,:realized_regret], normalized_regret=R[!, :normalized_regret], lower_bound=R[!,:lower_bound]))

# end

# for subdf in groupby(RESULTS_OSSB, :round)
#     round = subdf[1, :round][1]
#     CSV.write("Results/RESULTS-OSSB-"*string(round)*".csv", subdf)
# end

# file=matopen("CKL-UCB/CKL_UCB.mat")
# ckl_ucb=read(file, "normalized_regret")
# close(file)
# ckl_ucb = ckl_ucb[:, 1000:1000:10000]

# for i in 1:size(ckl_ucb)[2]
#     subdf = DataFrame(normalized_regret=ckl_ucb[:, i])
#     CSV.write("Results/RESULTS-CKL-UCB-"*string(i*1000)*".csv", subdf)
# end
