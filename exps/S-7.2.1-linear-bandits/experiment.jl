using ClusterManagers
using Distributed
using Parallelism

# Add in the cores allocated by the scheduler as workers
addprocs(SlurmManager(parse(Int,ENV["SLURM_NTASKS"])-1))
print("Added workers: ")
println(nworkers())

###########
## TASKS ##
###########
runs = 20
T = 10000

## BANDITS
bandits = filter(x->occursin(".mat", x), readdir("Bandits/5"))
simulations = hcat(repeat(bandits, inner=runs), repeat(1:runs, outer=size(bandits, 1)))
simulations = [[simulations[i, 1], simulations[i, 2]] for i in 1:size(simulations, 1)]

# Load Packages
@everywhere begin
    # import Pkg
    # Pkg.instantiate()
    import Random
    import DuSA
    using MAT
    using DataFrames
    using CSV
    using Logging
end

##############
## SIMULATE ##
##############
@everywhere function simulate(bandit, run, T)
    println("bandit=", bandit)
    println("run=", run)

    Random.seed!(32175+run)
    
    # Load Bandit
    file=matopen(joinpath("Bandits/5", bandit))
    P = read(file, "P")
    P=[P[x, :] for x in 1:size(P,1)]
    println("P=", P)    
    R = read(file, "R")
    println("R=", R)    
    X = read(file, "X")
    println("X=", X)        
    ηstar = read(file, "eta")
    close(file)
    
    B = DuSA.LinearBandit(P, R, X)
    println(B)

    io = open(joinpath("Logs", bandit*"-run-"*string(run)*".txt"), "w+")
    IOStream(joinpath("Logs", bandit*"-run-"*string(run)*".txt"))
    logger = SimpleLogger(io)
    global_logger(logger)

    if isfile(joinpath("Data", bandit*"-run-"*string(run)*".csv"))
        # Done
        println("This instance has already been completed")
    else
        # DuSA
        try
            r, x, N, η, μ, s, Bₒ, Δt, ϕ = DuSA.play(B, T)
            # Save
            regret = cumsum([DuSA.Δ(x[t], B) for t in 1:T])
            RESULTS = DataFrame(round=1:T, arm=x, reward=r, regret=regret, time=Δt, phase=ϕ)
            CSV.write(joinpath("Data", bandit*"-run-"*string(run)*".csv"), RESULTS)
        catch
            println("Error in DuSA simulation")
        end
    end
    close(io)
end

println("Begin Work:")
robust_pmap(simulation->simulate(simulation[1], simulation[2], T), simulations; num_retries=3)
