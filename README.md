[![pipeline status](https://gitlab.com/vanparys/dusa/badges/master/pipeline.svg)](https://gitlab.com/vanparys/dusa/commits/master)
[![coverage report](https://gitlab.com/vanparys/dusa/badges/master/coverage.svg)](https://gitlab.com/dusa/commits/master)

# Dual Structure Algorithm (DuSA)

Julia code associated with the multi-armed bandit [paper](https://arxiv.org/abs/2007.07302).

## Organization

The folder `exps` contains the code to replicate the numerical examples.

## Installation instructions

```julia
import Pkg
Pkg.add(url="https://gitlab.com/vanparys/dusa.git")
```
