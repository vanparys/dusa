struct BernoulliLipschitzBandit <: LipschitzBandit
    P
    R
    X
    L
end

function Base.show(io::IO, B::BernoulliLipschitzBandit)
    Printf.@printf(io=io, "Bernoulli Lipschitz bandit with K=%d arms embedded in Re^%d", length(B.P), size(B.X, 1))
end

"""
    explicit_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a lipschitz bandit B. 
"""
function explicit_target_rates(B::BernoulliLipschitzBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L
    
    K = length(P)
    N = length(R)
    d = size(X, 1)
 
    Xs, Xd, Xn = DuSA.partition(B, optimizer=optimizer)
    Xs, rew_star = DuSA.star(B)

    if length(Xd)==0
        # Nondeceitful bandit
        return [0 for x in 1:K]
    end
    
    # Optimization 
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]
    for xp in Xd                
        rewards = [maximum([DuSA.rew(x, B), rew_star-B.L*norm(B.X[:, x] - B.X[:, xp])]) for x in 1:K]
        Q = ([vcat([1 1], B.R')\[1; rewards[x]] for x in 1:K])                
        constrs +=
            [1 <= sum([η[x]*DuSA.I(B.P[x], Q[x]) for x in 1:K])]
    end
    
    obj = sum([η[x]*DuSA.Δ(x, B) for x in 1:K])
    problem = minimize(obj, constrs)
    
    solve!(problem, optimizer; silent_solver=true)
    
    return [maximum([η.value[x], 0]) for x in 1:K]
end

"""
    dual_from_primal(η, xp, D, B)

    Computes the optimal dual variable associated for the dual distance
    function for deceitful arm xp when optimal deceitful distribution 
    is given
"""
function dual_from_primal(η, xp, D::BernoulliLipschitzBandit, B::BernoulliLipschitzBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L
    
    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = Convex.Variable(1)
    β = Convex.Variable(1)
    λ = Convex.Variable(N, K)

    ######################
    ## DUAL FEASIBILITY ##
    ######################
    γ = Convex.Variable(K)
    Λ = Convex.Variable(K, K)
    constrs =
        [Λ[x, xp]>=0 for x in 1:K, xp in 1:K]
    constrs += # r=1
        [
            λ[2, x]==γ[x] + sum([Λ[xp, x]-Λ[x, xp] for xp in 1:K])
            for x in 1:K
        ] 
    constrs += # r=0
        [λ[1, x]==γ[x] for x in 1:K] 
    constrs +=
        [
            sum(γ)==L*sum(
                [norm(X[:, x] - X[:, xp])*Λ[x, xp]
                 for x in 1:K, xp in 1:K]
            )
        ]
    constrs +=
        [α>=0]

    ################
    ## Optimality ##
    ################
    constrs +=
        [η[x] - λ[r, x] - β - α*R[r]*(x==xp) == η[x] * B.P[x][r]/D.P[x][r] for x in 1:K, r in 1:N]      

    #####################
    ## Complementarity ##
    #####################
    obj = sum([Λ[x, xp]*max(0, L*norm(X[:, x] - X[:, xp]) - rew(x, D) + rew(xp, D)) for x in 1:K, xp in 1:K])


    ## MIN = 0 => Complementarity
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    ## Fail SAFE
    ## Happens if solver does not return a solution
    if isnothing(α.value)
        return 0, 0, zeros(N, K)
    end

    return α.value, β.value, λ.value
end

"""
    deep_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a lipschitz bandit B.
    
"""
function deep_target_rates(B::BernoulliLipschitzBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L

    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    if (length(Xs)==K || length(Xd)==0)
        # All arms optimal
        # OR
        # No deceitful arms
        η = [0 for x in 1:K]
        μ = DV(
            [0 for xp in 1:K],
            [0 for xp in 1:K],
            [zeros(N, K) for xp in 1:K]
        )
        return η, μ
    end
    
    #################
    ## SOLVE FOR η ##
    #################
    ηstar = DuSA.explicit_target_rates(B, optimizer=optimizer)

    #################
    ## SOLVE FOR μ ##
    #################
    αstar = [0.0 for xprime in 1:K]
    βstar = [0.0 for xprime in 1:K]
    λstar = [zeros(N, K) for xprime in 1:K]    

    Threads.@threads for xp in Xd
        rewards = [maximum([DuSA.rew(x, B), rew_star-B.L*norm(B.X[:, x] - B.X[:, xp])]) for x in 1:K]
        Q = ([vcat([1 1], B.R')\[1; rewards[x]] for x in 1:K])
        D = DuSA.BernoulliLipschitzBandit(Q, B.R, B.X, B.L)

        αstar[xp], βstar[xp], λstar[xp] = DuSA.dual_from_primal(ηstar, xp, D, B, optimizer=optimizer)
    end
    μstar = DV(αstar, βstar, λstar)

    return ηstar, μstar
end

"""
    update(B, P_new)

    Return updated bandit
    
"""
function update(B::BernoulliLipschitzBandit, P_new)
    return BernoulliLipschitzBandit(P_new, B.R, B.X, B.L)
end
