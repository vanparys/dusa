module DuSA

# Dependencies
using Convex
using JuMP
using Random
using StatsBase
using Mosek
using MosekTools

export DV
export Bandit
export update

# Dual variable
struct DV
    α
    β
    λ
end

Base.length(μ::DV) = length(μ.α)
Base.getindex(μ::DV, i) = DV(μ.α[i], μ.β[i], μ.λ[i])
function Base.setindex!(μ::DV, μi::DV, i)
    μ.α[i]=μi.α
    μ.β[i]=μi.β
    μ.λ[i]=μi.λ
end

"""
    Abstract bandit instance in a bandit problem class
"""
abstract type Bandit end

# Export Functions
export rew
export play
export star
export Δ
export reward_max
export partition
export cp_target_rates
export dist_function_primal
export dist_function_dual
export test_function
export shallow_target_rates
export mle
export deep_target_rates
export round


"""
    rew(x, B)

    Return expected reward of arm `x` for bandit `B`
"""
function rew(x, B::Bandit)
    # Bandit
    P = B.P
    R = B.R
    
    N = length(R)

    return sum([P[x][r]*R[r] for r=1:N])
end

"""
    pull(x, B)

    Return random reward of arm `x` for bandit `B`
"""
function pull(x, B::Bandit)
    # Bandit
    P = B.P
    R = B.R
    
    return sample(R, Weights(P[x]))
end

"""
    play(B, T)

    Play bandit for T >= K rounds 
"""
function play(B::Bandit, T; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)
    
    # DUSA INITIALIZE
    r = Array{Union{Float64, Missing}}(missing, T)
    x = Array{Union{Int, Missing}}(missing, T)
    N = Array{Union{Array{Union{Int, Missing}}, Missing}}(missing, T+1)
    η = Array{Union{Array{Union{Float64, Missing}}, Missing}}(missing, T+1)
    μ = Array{Union{DV, Missing}}(missing, T+1)
    s = Array{Union{Int, Missing}}(missing, T+1)
    Bₒ= Array{Union{Bandit, Missing}}(missing, T+1)
    Δt = Array{Union{Float64, Missing}}(missing, T)
    ϕ = Array{Union{String, Missing}}(missing, T)
    for t in 1:K   
        x[t] = t
        r[t] = pull(x[t], B)
        Δt[t] = 0
        ϕ[t] = "Initialization"
    end    
    Bₒ[K+1] = update(B, [1.0*(B.R.==r[t]) for t=1:K])
    N[K+1]  = ones(K)
    s[K+1]  = 0
    η[K+1], μ[K+1] = deep_target_rates(mle(Bₒ[K+1], N[K+1], optimizer=optimizer), optimizer=optimizer)

    # DUSA MAIN
    for t in K+1:T        
        ts = @timed begin            
            x[t], μ[t+1], s[t+1], η[t+1], ϕ[t]  = round(Bₒ[t], N[t], μ[t], s[t], η[t], t, optimizer=optimizer)
        end
        Δt[t] = ts[2]
        
        # PULL
        r[t] = pull(x[t], B)

        # Update P_t
        Pₜ = deepcopy(Bₒ[t].P)
        Pₜ[x[t]] = (Pₜ[x[t]]*N[t][x[t]].+(Bₒ[t].R.==r[t]))/(N[t][x[t]]+1)
        Bₒ[t+1] = update(Bₒ[t], Pₜ)

        # Update N_t
        N[t+1] = deepcopy(N[t])
        N[t+1][x[t]] = N[t][x[t]]+1        
    end
    
    return r, x, N, η, μ, s, Bₒ, Δt, ϕ
end

"""
    round(B::Bandit, N, μ::DV, s, t; ε=1e-3)

    Carries out one round of DuSA algorithm
"""
function round(B_t::Bandit, N_t, μ_t::DV, s_t, η_t, t; ε=1e-3, verbose=true, optimizer=Mosek.Optimizer)

    if(verbose)
        println("******************")
        println("DUSA round t=", t)
        println("******************")
    end

    # Compute ML estimate (eventually B_mle ≈ B_t)
    B_mle = mle(B_t, N_t; optimizer=optimizer)
    
    # Bandit
    P_t = B_mle.P
    R = B_mle.R

    K = length(P_t)
    N = length(R)
    
    # Partition arms
    Xs, Xd, Xn = DuSA.partition(B_mle, optimizer=optimizer)

    if(verbose)
        println("Empirical Optimal Arm:", Xs)
        println("Deceitful Arms:", Xd)
        println("Non-Deceitful Arms:", Xn)
    end
    
    # Sufficient Information Test
    dual_bar = ones(K).+ε
    Threads.@threads for xp in Xd
        dual_bar[xp] = test_function(N_t./log(t), xp, B_mle, μ_t)
    end

    if(verbose)
        println("Test Function: ", dual_bar[Xd])
        println("Desired Separtion: ", (1+ε)*(1-exp(-t/2000)))
    end
    
    μ = μ_t
    s = s_t
    η = η_t
    
    x_t = 1
    if (minimum(dual_bar) >= (1+ε)*(1-exp(-t/2000)))
        # Exploitation        
        x_t = argmax([rew(x, B_mle) for x in 1:K])
        ϕ = "Exploitation"
        if(verbose)
            println("Phase: Exploitation")
            println("Pull arm x_t=", x_t)
        end
    else
        s = s_t + 1
        if(verbose)
            println("Exploration round s_t=", s_t)
            println("Minimum N_t=", minimum(N_t))
            println("Minimum Req.=", 0.01*s_t/(log(s_t+1)))
        end
        if (minimum(N_t)<=0.01*s_t/(log(s_t+1)))
            # Estimation
            x_t = argmin(N_t)
            ϕ = "Estimation"
            if(verbose)
                println("Phase: Estimation")
                println("Pull arm x_t=", x_t)
            end
        else
            η_t, ρ = shallow_target_rates(B_mle, μ_t)
            X_c = findall(s->s<(1+ε)*(1-exp(-t/2000)), N_t./(η_t*log(t)))
            if(verbose)           
                println("Shallow update η_t*log(t)=", η_t*log(t))
                println("N_t=", N_t)
                println("Relative difference N_t/η_t=", N_t./(η_t*log(t)))
                println("X_c=", X_c)
            end
            if (minimum(N_t./(η_t*log(t)))>=(1+ε)*(1-exp(-t/2000)) || N_t[argmax([rew(x, B_mle) for x in 1:K])] <= maximum(N_t[X_c]))
                # Exploitation
                x_t = argmax([rew(x, B_mle) for x in 1:K])
                ϕ = "Exploration/Exploitation"
                if(verbose)
                    println("Phase: Exploration - Exploitation")
                end
            else
                # Exploration                
                x_t = X_c[argmin(N_t[X_c])]
                ϕ = "Exploration/Exploration"
                if(verbose)
                    println("Phase: Exploration - Exploration")                    
                end
            end
            #################
            ## Deep Update ##
            #################
            η, μ = deep_target_rates(B_mle, optimizer=optimizer)
            if(verbose)
                println("Pull arm x_t=", x_t)
                println("Deep update η*log(t)=", η*log(t))
            end    
        end
    end
    
    return x_t, μ, s, η, ϕ
end

"""
    star(B; ε=1e-4)

    Return optimal arms and reward of bandit B
"""
function star(B::Bandit; ε = 1e-4)
    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    rew_star = maximum([rew(x, B) for x in 1:K])
    # Return arms which are optimal up to ε
    Xs = findall(s->s>=rew_star-ε, [rew(x, B) for x in 1:K])
    
    return Xs, rew_star
end

"""
    Δ(x, B)

    Return suboptimality gap of arm `x` in bandit `B`
"""
function Δ(x, B::Bandit)
    # Bandit
    P = B.P
    R = B.R

    Xs, rew_star = star(B)

    return maximum([rew_star - rew(x, B), 0])
end

"""
    partition(B; optimizer=Mosek.Optimizer, ε=1e-4)

    Given a bandit instance `B` with reward distribution `B.P` supported on
    outcome set `B.R` with arms `B.X`, partition the arms as
    - optimal (Xs), 
    - deceiving (Xd),
    - nondeceiving (Xn).
"""
function partition(B::Bandit; optimizer=Mosek.Optimizer, ε = 1e-4)
    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)
    
    # Optimal arm/reward
    Xs, rew_star = star(B, ε = ε)
    
    # Partition
    Xd = setdiff(findall(x-> reward_max(x, B, optimizer=optimizer) > rew_star*(1+ε), 1:K), Xs)
    Xn = setdiff(1:K, Xs)
    Xn = setdiff(Xn, Xd)

    return Xs, Xd, Xn
end


"""
    cp_target_rates(B; η_min=1e-2)

    Computes the optimal target rates η with which to pull suboptimal
    arms using a cutting plane formulation. The constraint η ≥ η_min 
    helps with the numerical speed of the algorithm.
"""
function cp_target_rates(B::Bandit; η_min=1e-2, max_cp=20, ε=1e-4, verbose=false, optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)

    # Partition
    Xs, Xd, Xn = partition(B; optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    # No deceitful arms
    if length(Xd)==0
        return zeros(K), 0
    end
    
    p = Model(optimizer)
    set_silent(p)
    @variable(p, η[x=1:K] >= η_min)
    for xs in Xs
        @constraint(p, η[xs] == η_min)
    end
    @objective(p, Min, sum([η[x]*Δ(x, B) for x in 1:K]))
        
    acc = 0 #Accuracy
    η_k = zeros(K)
    c_ub = Inf
    μ = [DV(0, 0, zeros(N, K)) for x in 1:K]
    for k in 1:max_cp
        optimize!(p)
        
        # Polish solution
        η_k = [maximum([value.(η)[x], 0]) for x in 1:K]
        
        D = zeros(K)
        A = [zeros(K) for x in 1:K]

        Threads.@threads for xp in Xd
            D[xp], Qs, s = dist_function_primal(η_k, xp, B, optimizer=optimizer)        
            # Add cut
            A[xp] = [I(B.P[x], Qs[x]) for x in 1:K]
            D[xp] = dot(A[xp], η_k)
        end
        
        cuts = 0
        for xp in Xd
            if minimum(A[xp])>=0                 
                @constraint(p, sum([η[x]*A[xp][x] for x in 1:K])>=1)
                if sum([η_k[x]*A[xp][x] for x in 1:K])<1
                    cuts = cuts+1
                end                
            end
        end
        Dx = D[Xd]

        acc = minimum(Dx[findall(s->s>=0, Dx)])
        c_ul = sum([η_k[x]*Δ(x, B) for x in 1:K])
        c_ub = minimum([c_ub, c_ul/acc ])
        if (verbose)
            println("k = ", k)
            println("acc = ", ε)

            println("c_lb=", c_ul)
            println("c_ub=", c_ub)
        end

        if acc > 1-ε
            if (verbose)
                println("sufficient accuracy")
            end
            break
        end

        if cuts==0
            if (verbose)
                println("no progress")
            end
            break
        end
    end

    return η_k, ε
end

function dual_function(η, xp, B, μ)

    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = μ.α[xp]
    β = μ.β[xp]
    λ = μ.λ[xp]
    
    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*my_relative_entropy(η[x],η[x]-λ[r, x]-β-α*R[r]*(x==xp))
            for r in 1:N, x in intersect(Xt, findall(s->s>0, η))
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x] for r in 1:N, x in Xs
        ]
    )+  α*rew_star+β*length(Xt)

    return dualf
end


function grad_dual_function(η, xp, B, ρ, μ)
    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = μ.α[xp]
    β = μ.β[xp]
    λ = μ.λ[xp]
    
    grad_dualf =
        sum(
        [
            -P[x][r]*η[x]*(λ[r, x]+β+α*R[r]*(x==xp))/(η[x]-ρ*λ[r, x]-ρ*β-ρ*α*R[r]*(x==xp))
            for r in 1:N, x in intersect(Xt, findall(s->s>0, η))
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+ α*rew_star+β*length(Xt)

    return grad_dualf
end

"""
    test_function(η, xp, B, μ)

    Computes the statistical distance between B.P and its deceitful 
    distributions concerning arm xp using a dual minimization 
    formulation.
"""
function test_function(η, xp, B::Bandit, μ::DV; ε=1e-2)

    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    α = μ.α[xp]
    β = μ.β[xp]
    λ = μ.λ[xp]    

    ds = [λ[r, x] + β + α*R[r]*(x==xp) for r in 1:N, x=1:K]
    ρs = [η[x]/(λ[r, x] + β + α*R[r]*(x==xp)) for r in 1:N, x=1:K]
    ρmin = 0
    if length(ρs[findall(s->s>1e-8, ds)])==0
        ρmax = 10
    else
        ρmax = minimum([10, minimum(ρs[findall(s->s>1e-8, ds)])])
    end

    # Bisection
    while ρmax-ρmin>ε
        ρ = (ρmin+ρmax)/2
        g = grad_dual_function(η, xp, B, ρ, μ)
        if g<=0
            ρmax = ρ
        else
            ρmin = ρ
        end        
    end

    ρ = (ρmin+ρmax)/2    
    return dual_function(η, xp, B, DV(ρ*μ.α, ρ*μ.β, ρ*μ.λ))
end


"""
    shallow_target_rates(B, μ)

    Performs the shallow update
    
    @see Shallow Update Algorithm
"""
function shallow_target_rates(B::Bandit, μ::DV; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    # Primal variables
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]
    ρ = Convex.Variable(K)
    constrs +=
        [ρ[x]>=0 for x in 1:K]
    
    # Dual variables
    α = μ.α
    β = μ.β
    λ = μ.λ
    
    # Minimize the regret
    obj = sum([η[x]*Δ(x, B) for x in 1:K])

    # Dual functions
    dualfs = [sum(
        [
            -P[x][r]*relative_entropy(
                η[x], η[x]-ρ[xp]*λ[xp][r,x]-ρ[xp]*β[xp] - ρ[xp]*α[xp]*R[r]*(x==xp))
            for r in 1:N, x in Xt
        ]
    )+sum(
        [
            -P[x][r]*ρ[xp]*λ[xp][r, x]
            for r in 1:N, x in Xs
        ]
    )+ρ[xp]*α[xp]*rew_star+ρ[xp]*β[xp]*length(Xt)
              for xp in 1:K
    ]
    # Implicit Dual function constraints
    constrs +=
        [η[x] >= ρ[xp]*λ[xp][r, x] + ρ[xp]*β[xp] + ρ[xp]*α[xp]*R[r]*(x==xp) for r in 1:N, x=1:K, xp=1:K]

    # Sufficient exploration
    constrs +=
        [dualfs[xp]>=1 for xp in Xd]

    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    # Polish solution
    if ( η.value != nothing )
        η = [maximum([η.value[x], 0]) for x in 1:K]
    else
        println("Shallow update failed")
        η = [Inf for x in 1:K]
    end
        
    return η, ρ.value
end

# Generic Bandits
include("generic-bandits.jl")

# Linear Bandits
include("linear-bandits.jl")

# Lipschitz Bandits
include("lipschitz-bandits.jl")
# Bernoulli Lipschitz Bandits
include("bernoulli-lipschitz-bandits.jl")
# General Lipschitz Bandits
include("general-lipschitz-bandits.jl")

# Wasserstein Bandits
include("wasserstein-bandits.jl")

# Dispersion Bandits
include("dispersion-bandits.jl")


export I

"""
    I(P, Q)

    Returns relative entropy or KL divergence between P and Q
"""
function I(P, Q)

    d = length(P)
    D = Inf

    if(minimum(P)>=0 && minimum(Q)>=0)# && abs(sum(P)-1)<=1e-2 && abs(sum(Q)-1)<=1e-2)
        D = 0
        for i = 1:d
            if (P[i]>0 && Q[i]>0)
                D = D + P[i]*log(P[i]/Q[i])
            elseif (P[i]>0 && Q[i]==0) 
                D = Inf
            end
        end
    end

    # Clipping avoids numerical issues
    return minimum([maximum([D, 0]), 1e5])
end

function my_relative_entropy(a, b)

    if a==0
        return 0
    end

    if b==0
        return Inf
    end
    
    return a*log(a/b)
    
end

# Dispersion Bandits
include("klUCB.jl")
include("OSSB.jl")


end # module
