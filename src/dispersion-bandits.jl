struct DispersionBandit <: Bandit
    P
    R
    γ
end

import Printf

function Base.show(io::IO, B::DispersionBandit)
    Printf.@printf(io=io, "Dispersion bandit with K=%d arms for R=%d outcomes", length(B.P), length(B.R))
end

"""
    reward_max(xp, B)
    
    Given a dispersion bandit B with reward distribution B.P supported 
    on outcome set B.R compute the maximum conceivable 
    award of arm xp assuming dispersion structure
"""
function reward_max(xp, B::DispersionBandit; optimizer = Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    
    N = length(R)
    K = length(P)
  
    Xs, rew_star = star(B)
    
    # Distributional Optimization
    Q = Convex.Variable(N, K)
    
    constrs =
        [Q>=0]
    for xs in Xs
        constrs += [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [
          sum([R[r]^2*Q[r, x] for r in 1:N]) <= B.γ[x]*sum([R[r]*Q[r, x] for r in 1:N]) for x in 1:K
        ]

    obj = sum([Q[r, xp]*R[r] for r in 1:N])

    problem = maximize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
    
    return problem.optval
end

"""
    dist_function_primal(η, xp, B)

    Computes the statistical distance between B.P and its deceitful 
    distributions concerning arm xp using a primal minimization 
    formulation.
"""
function dist_function_primal(η, xp, B::DispersionBandit; verbose=false, optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)

    # Best reward
    Xs, rew_star = star(B)
    
    if (verbose)
        println("***************************")
        println(" Distance Function (Primal)")
        println("***************************")
        println("η=", η)
        println("P=", P)
        println("γ=", B.γ)
        println("Optimal Arms: ", Xs)
        println("Optimal reward: ", rew_star)
    end

    # Distributional Optimization
    Q = Convex.Variable(N, K)
    
    constrs =
        [Q>=0]
    for xs in Xs
        constrs += [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [
          sum([R[r]^2*Q[r, x] for r in 1:N]) <= B.γ[x]*sum([R[r]*Q[r, x] for r in 1:N]) for x in 1:K
        ]
    constrs +=
        [sum([Q[r, xp]*R[r] for r in 1:N]) >= rew_star]

    obj = sum([η[x]*relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)

    solve!(problem, optimizer; silent_solver=true)

    if verbose
        println("problem=", problem)
        println("Q=",[Q.value[:, x] for x in 1:K])
    end
    
    return problem.optval, [Q.value[:, x] for x in 1:K], problem.status
end

"""
    dist_function_dual(η, xp, B)

    Computes the statistical distance between B.P and its deceitful
    distributions concerning arm xp using a dual minimization 
    formulation.
"""
function dist_function_dual(η, xp, B::DispersionBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)
    
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = Convex.Variable(1)
    β = Convex.Variable(1)
    λ = Convex.Variable(N, K)

    # Dual Cone λ∈Kstar
    γ = Convex.Variable(K)
    Λ = Convex.Variable(K)

    constrs =
        [Λ[x]>=0 for x in 1:K]
    constrs += 
        [
            λ[r, x] - γ[x] + (R[r]^2-B.γ[x]*R[r])*Λ[x] >= 0
            for r in 1:N, x in 1:K
        ]
    constrs +=
        [
            sum(γ)==0
        ]
    
    # Other Dual constraints
    constrs +=
        [α>=0]
    constrs +=
        [η[x] >= λ[r, x] + β + α*R[r]*(x==xp) for r in 1:N, x=1:K]

    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*relative_entropy(
                Constant(η)[x],η[x]-λ[r, x]-β- α*R[r]*(x==xp)
            )
            for r in 1:N, x in Xt
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+  α*rew_star+β*length(Xt)

    problem = maximize(dualf, constrs)
    solve!(problem, optimizer; silent_solver=true)

    return problem.optval
end

"""
    deep_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a dispersion bandit B.
    
"""
function deep_target_rates(B::DispersionBandit; optimizer=Mosek.Optimizer)
    
    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    if (length(Xs)==K)
        # All arms optimal
        η = [0 for x in 1:K]
        μ = DV(
            [0 for xp in 1:K],
            [0 for xp in 1:K],
            [zeros(N, K) for xp in 1:K]
        )
        return η, μ
    end
    
    # Primal variables
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]

    # Minimize the regret
    obj = sum([η[x]*Δ(x, B) for x in 1:K])

    # Dual variables (α∈ℜ₊, β∈ℜ)
    α = [Convex.Variable(1) for xprime in 1:K]
    constrs +=
        [α[xprime]>=0 for xprime in 1:K]
    β = [Convex.Variable(1) for xprime in 1:K]


    # Dual Cone λ∈Kstar
    λ = [Convex.Variable(N, K) for xprime in 1:K]
    γ = [Convex.Variable(K) for xprime in 1:K]
    Λ = [Convex.Variable(K) for xprime in 1:K]

    constrs +=
        [Λ[xprime][x]>=0 for x in 1:K, xprime in 1:K]
    constrs += 
        [
            λ[xprime][r, x] - γ[xprime][x] + (R[r]^2-B.γ[x]*R[r])*Λ[xprime][x] >= 0
            for r in 1:N, x in 1:K, xprime in 1:K
        ]
    constrs +=
        [
            sum(γ[xprime])==0 for xprime in 1:K
        ]    

    # Dual functions
    dualfs = [sum(
        [
            -P[x][r]*relative_entropy(
                η[x], η[x]-λ[xp][r,x]-β[xp] - α[xp]*R[r]*(x==xp))
            for r in 1:N, x in Xt
        ]
    )+sum(
        [
            -P[x][r]*λ[xp][r, x]
            for r in 1:N, x in Xs
        ]
    )+α[xp]*rew_star+β[xp]*length(Xt)
    for xp in 1:K
    ]

    # Implicit Dual function constraints
    constrs +=
        [
            η[x] >= λ[xp][r, x] + β[xp] + α[xp]*R[r]*(x==xp)
            for r in 1:N, x=1:K, xp=1:K
        ]

    # Sufficient exploration
    constrs +=
        [dualfs[xp]>=1 for xp in Xd]

    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    if !isnothing(η.value)
        # Polish solution
        η = [maximum([η.value[x], 0]) for x in 1:K]
        μ = DV(
            [α[xp].value for xp in 1:K],
            [β[xp].value for xp in 1:K],
            [λ[xp].value for xp in 1:K]
        )
    else
        # Fall-Back Option if Mosek can not find solution
        η, μ = deep_target_rates(GenericBandit(B.P, B.R))
    end
    
    return η, μ
end


"""
    mle(B, N)

    Computes the statistical projection of B onto the Wasserstein bandit class
"""
function mle(B::DispersionBandit, N_t; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)
    
    # Distributional projection
    Q = Convex.Variable(N, K)

    constrs =
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [
            sum([R[r]^2*Q[r, x] for r in 1:N]) <= B.γ[x]*sum([R[r]*Q[r, x] for r in 1:N]) for x in 1:K
        ]

    obj = sum([N_t[x]*relative_entropy(Constant(B.P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
    
    return DispersionBandit([Q.value[:, x] for x in 1:K], R, B.γ)

end

"""
    update(B, P_new)

    Return updated bandit
    
# """
function update(B::DispersionBandit, P_new)
    return DispersionBandit(P_new, B.R, B.γ)
end
