struct WassersteinBandit <: Bandit
    P
    R
    C
    D
end

import Printf

function Base.show(io::IO, B::WassersteinBandit)
    Printf.@printf(io=io, "Wasserstein bandit with K=%d arms for R=%d outcomes", length(B.P), length(B.R))
end

"""
    reward_max(xp, B)
    
    Given a Wasserstein bandit B with reward distribution B.P supported 
    on outcome set B.R compute the maximum conceivable 
    award of arm xp assuming Wasserstein structure
"""
function reward_max(xp, B::WassersteinBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    
    N = length(R)
    K = length(P)
    C = size(B.C, 1)

    Xs, rew_star = star(B)
    
    # Distributional Optimization
    Q = Convex.Variable(N, K)
    T = [Convex.Variable(N, N) for c in 1:C]
    
    constrs =
        [Q>=0]
    for xs in Xs
        constrs += [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [T[c]>=0 for c in 1:C]
    constrs +=
        [
            sum([abs(R[r1]-R[r2])*T[c][r1, r2] for r1 in 1:N, r2 in 1:N]) <= B.D[c] for c in 1:C
        ]
    for c in 1:C
        constrs +=
            [
                sum([T[c][r1, r2] for r2 in 1:N])==Q[r1, x] for r1 in 1:N, x in B.C[c, 1]
            ]
        constrs +=
            [
                sum([T[c][r1, r2] for r1 in 1:N])==Q[r2, x] for r2 in 1:N, x in B.C[c, 2]
            ]
    end

    obj = sum([Q[r, xp]*R[r] for r in 1:N])

    problem = maximize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
    
    return problem.optval
end

"""
    dist_function_primal(η, xp, B)

    Computes the statistical distance between B.P and its deceitful 
    distributions concerning arm xp using a primal minimization 
    formulation.
"""
function dist_function_primal(η, xp, B::WassersteinBandit; optimizer=Mosek.Optimizer, verbose=false)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)
    C = size(B.C, 1)

    # Best reward
    Xs, rew_star = star(B)
    
    if (verbose)
        println("***************************")
        println(" Distance Function (Primal)")
        println("***************************")
        println("η=", η)
        println("P=", P)
        println("C=", B.C)
        println("D=", B.D)
        println("Optimal Arms: ", Xs)
        println("Optimal reward: ", rew_star)
    end

    # Distributional Optimization
    Q = Convex.Variable(N, K)
    T = [Convex.Variable(N, N) for c in 1:C]
    
    constrs =
        [Q>=0]
    for xs in Xs
        constrs += [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [T[c]>=0 for c in 1:C]
    constrs +=
        [
            sum([abs(R[r1]-R[r2])*T[c][r1, r2] for r1 in 1:N, r2 in 1:N]) <= B.D[c] for c in 1:C
        ]
    for c in 1:C
        constrs +=
            [
                sum([T[c][r1, r2] for r2 in 1:N])==Q[r1, x] for r1 in 1:N, x in B.C[c, 1]
            ]
        constrs +=
            [
                sum([T[c][r1, r2] for r1 in 1:N])==Q[r2, x] for r2 in 1:N, x in B.C[c, 2]
            ]
    end
    constrs +=
        [sum([Q[r, xp]*R[r] for r in 1:N]) >= rew_star]

    obj = sum([η[x]*relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)

    solve!(problem, optimizer; silent_solver=true)

    if verbose
        println("problem=", problem)
        println("Q=",[Q.value[:, x] for x in 1:K])
    end
    
    return problem.optval, [Q.value[:, x] for x in 1:K], problem.status
end

"""
    dist_function_dual(η, xp, B)

    Computes the statistical distance between B.P and its deceitful
    distributions concerning arm xp using a dual minimization 
    formulation.
"""
function dist_function_dual(η, xp, B::WassersteinBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)
    C = size(B.C, 1)
    
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = Convex.Variable(1)
    β = Convex.Variable(1)
    λ = Convex.Variable(N, K)

    # Dual Cone λ∈Kstar
    γ = Convex.Variable(K)
    Λ = Convex.Variable(C)
    f = Convex.Variable(C, N)

    constrs =
        [Λ[c]>=0 for c in 1:C]
    constrs +=
        [abs(f[c, r]-f[c, rp]) <= Λ[c]*abs(R[r]-R[rp]) for r in 1:N, rp in 1:N, c in 1:C]
    constrs += 
        [
            λ[r, x] - γ[x] +
            sum([f[c, r] for c in findall(s->s==x, [B.C[c, 1] for c in 1:C])]) -
            sum([f[c, r] for c in findall(s->s==x, [B.C[c, 2] for c in 1:C])])>=0
            for r in 1:N, x in 1:K
        ]
    constrs +=
        [
            sum(γ)==sum([B.D[c]*Λ[c] for c in 1:C])
        ]
    
    # Other Dual constraints
    constrs +=
        [α>=0]
    constrs +=
        [η[x] >= λ[r, x] + β + α*R[r]*(x==xp) for r in 1:N, x=1:K]

    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*relative_entropy(
                Constant(η)[x],η[x]-λ[r, x]-β- α*R[r]*(x==xp)
            )
            for r in 1:N, x in Xt
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+  α*rew_star+β*length(Xt)

    problem = maximize(dualf, constrs)

    solve!(problem, optimizer; silent_solver=true)

    return problem.optval
end

"""
    deep_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a wasserstein bandit B.
    
"""
function deep_target_rates(B::WassersteinBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)
    C = size(B.C, 1)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)
    

    # Primal variables
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]

    # Minimize the regret
    obj = sum([η[x]*Δ(x, B) for x in 1:K])

    # Dual variables (α∈ℜ₊, β∈ℜ)
    α = [Convex.Variable(1) for xprime in 1:K]
    constrs +=
        [α[xprime]>=0 for xprime in 1:K]
    β = [Convex.Variable(1) for xprime in 1:K]


    # Dual Cone λ∈Kstar
    λ = [Convex.Variable(N, K) for xprime in 1:K]
    γ = [Convex.Variable(K) for xprime in 1:K]
    Λ = [Convex.Variable(K, K) for xprime in 1:K]
    f = [Convex.Variable(C, N) for xprime in 1:K]
    constrs +=
        [Λ[xprime][c]>=0 for c in 1:C, xprime in 1:K]
    constrs +=
        [abs(f[xprime][c, r]-f[xprime][c, rp]) <= Λ[xprime][c]*abs(R[r]-R[rp]) for r in 1:N, rp in 1:N, c in 1:C, xprime in 1:K]
    constrs += 
        [
            λ[xprime][r, x] - γ[xprime][x] +
            sum([f[xprime][c, r] for c in findall(s->s==x, [B.C[c, 1] for c in 1:C])]) -
            sum([f[xprime][c, r] for c in findall(s->s==x, [B.C[c, 2] for c in 1:C])])>=0
            for r in 1:N, x in 1:K, xprime in 1:K
        ]
    constrs +=
        [
            sum(γ[xprime])==sum([B.D[c]*Λ[xprime][c] for c in 1:C]) for xprime in 1:K
        ]

    # Dual functions
    dualfs = [sum(
        [
            -P[x][r]*relative_entropy(
                η[x], η[x]-λ[xp][r,x]-β[xp] - α[xp]*R[r]*(x==xp))
            for r in 1:N, x in Xt
        ]
    )+sum(
        [
            -P[x][r]*λ[xp][r, x]
            for r in 1:N, x in Xs
        ]
    )+α[xp]*rew_star+β[xp]*length(Xt)
    for xp in 1:K
    ]

    # Implicit Dual function constraints
    constrs +=
        [
            η[x] >= λ[xp][r, x] + β[xp] + α[xp]*R[r]*(x==xp)
            for r in 1:N, x=1:K, xp=1:K
        ]

    # Sufficient exploration
    constrs +=
        [dualfs[xp]>=1 for xp in Xd]

    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    # Polish solution
    η = [maximum([η.value[x], 0]) for x in 1:K]
    μ = DV(
        [α[xp].value for xp in 1:K],
        [β[xp].value for xp in 1:K],
        [λ[xp].value for xp in 1:K]
    )
    
    return η, μ
end


"""
    mle(B, N)

    Computes the statistical projection of B onto the Wasserstein bandit class
"""
function mle(B::WassersteinBandit, N_t; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    N = length(R)
    K = length(P)
    C = size(B.C, 1)
    
    # Distributional projection
    Q = Convex.Variable(N, K)
    T = [Convex.Variable(N, N) for c in 1:C]
    
    constrs =
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [T[c]>=0 for c in 1:C]
    constrs +=
        [
            sum([abs(R[r1]-R[r2])*T[c][r1, r2] for r1 in 1:N, r2 in 1:N]) <= B.D[c] for c in 1:C
        ]
    for c in 1:C
        constrs +=
            [
                sum([T[c][r1, r2] for r2 in 1:N])==Q[r1, x] for r1 in 1:N, x in B.C[c, 1]
            ]
        constrs +=
            [
                sum([T[c][r1, r2] for r1 in 1:N])==Q[r2, x] for r2 in 1:N, x in B.C[c, 2]
            ]
    end
    
    obj = sum([N_t[x]*relative_entropy(Constant(B.P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    return WassersteinBandit([Q.value[:, x] for x in 1:K], R, B.C, B.D)

end

"""
    update(B, P_new)

    Return updated bandit
    
# """
function update(B::WassersteinBandit, P_new)
    return WassersteinBandit(P_new, B.R, B.C, B.D)
end
