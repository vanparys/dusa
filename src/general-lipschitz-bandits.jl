struct GeneralLipschitzBandit <: LipschitzBandit
    P
    R
    X
    L
end

"""
    update(B, P_new)

    Return updated bandit
    
"""
function update(B::GeneralLipschitzBandit, P_new)
    return GeneralLipschitzBandit(P_new, B.R, B.X, B.L)
end
