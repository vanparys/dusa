abstract type LipschitzBandit <: Bandit end

import Printf

function Base.show(io::IO, B::LipschitzBandit)
    Printf.@printf(io=io, "Lipschitz bandit with K=%d arms embedded in Re^%d and R=%d outcomes", length(B.P), size(B.X, 1), length(B.R))
end

"""
    reward_max(xp, B)
    
    Given a L-Lipschitz bandit B with reward distribution B.P supported 
    on outcome set B.R with arms B.X compute the maximum conceivable 
    award of arm xp assuming lipschitz structure
"""
function reward_max(xp, B::LipschitzBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L
    
    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Best arm
    Xs, rew_star = star(B)

    Q = Convex.Variable(N, K)
    m = Convex.Variable(K)

    constrs  =
        [m[x] == sum([Q[r, x]*R[r] for r in 1:N]) for x in 1:K]
    constrs +=
        [
            m[x1]-m[x2]<=L*norm(X[:, x1]-X[:, x2])
            for x1 in 1:K, x2 in 1:K
        ]
    constrs +=
        [Q>=0]
    for xs in Xs
        constrs +=
            [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in setdiff(1:K, Xs)]

    problem = maximize(m[xp], constrs)
    solve!(problem, optimizer; silent_solver=true)
    
    return problem.optval
end

"""
    dist_function_primal(η, xp, B)

    Computes the statistical distance between B.P and its deceitful 
    distributions concerning arm xp using a primal minimization 
    formulation.
"""
function dist_function_primal(η, xp, B::LipschitzBandit; optimizer=Mosek.Optimizer, verbose=false)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L

    K = length(P)
    N = length(R)

    # Best reward
    Xs, rew_star = star(B)
    
    if (verbose)
        println("***************************")
        println(" Distance Function (Primal)")
        println("***************************")
        println("η=", η)
        println("P=", P)
        println("R=", X)
        println("L=", L)
        println("Optimal Arms: ", Xs)
        println("Optimal reward: ", rew_star)
    end
        
    Q = Convex.Variable(N, K)
    m = Convex.Variable(K)

    constrs  =
        [m[x] == sum([Q[r, x]*R[r] for r in 1:N]) for x in 1:K]

    constrs +=
        [
            m[x1]-m[x2]<=L*norm(X[:, x1]-X[:, x2])
            for x1 in 1:K, x2 in 1:K
        ]
    constrs +=
        [Q>=0]
    for xs in Xs
        constrs +=
            [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in setdiff(1:K, Xs)]

    constrs +=
        [m[xp]>=rew_star]

    obj = sum([η[x]*relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)

    solve!(problem, optimizer; silent_solver=true)

    if verbose
        println("problem=", problem)
        println("Q=",[Q.value[:, x] for x in 1:K])
    end
    
    return problem.optval, [Q.value[:, x] for x in 1:K], problem.status
end

"""
    dist_function_dual(η, xp, B)

    Computes the statistical distance between B.P and its deceitful
    distributions concerning arm xp using a dual maximization 
    formulation.
"""
function dist_function_dual(η, xp, B::LipschitzBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L
    
    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = Convex.Variable(1)
    β = Convex.Variable(1)
    λ = Convex.Variable(N, K)

    # Dual Cone λ∈Kstar
    γ = Convex.Variable(K)
    Λ = Convex.Variable(K, K)
    constrs =
        [Λ[x, xp]>=0 for x in 1:K, xp in 1:K]
    constrs += # r=1
        [
            λ[2, x]>=γ[x] + sum([Λ[xp, x]-Λ[x, xp] for xp in 1:K])
            for x in 1:K
        ] 
    constrs += # r=0
        [λ[1, x]>=γ[x] for x in 1:K] 
    constrs +=
        [
            sum(γ)==L*sum(
                [norm(X[:, x] - X[:, xp])*Λ[x, xp]
                 for x in 1:K, xp in 1:K]
            )
        ]
    
    # Other Dual constraints
    constrs +=
        [α>=0]
    constrs +=
        [η[x] >= λ[r, x] + β + α*R[r]*(x==xp) for r in 1:N, x=1:K]

    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*relative_entropy(
                Constant(η)[x],η[x]-λ[r, x]-β- α*R[r]*(x==xp)
            )
            for r in 1:N, x in Xt
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+  α*rew_star+β*length(Xt)

    problem = maximize(dualf, constrs)
    solve!(problem, optimizer; silent_solver=true)

    return problem.optval
end

"""
    deep_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a lipschitz bandit B.
    
"""
function deep_target_rates(B::LipschitzBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L

    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    if (length(Xs)==K)
        # All arms optimal
        η = [0 for x in 1:K]
        μ = DV(
            [0 for xp in 1:K],
            [0 for xp in 1:K],
            [zeros(N, K) for xp in 1:K]
        )
        return η, μ
    end
    
    # Primal variables
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]

    # Minimize the regret
    obj = sum([η[x]*Δ(x, B) for x in 1:K])

    # Dual variables (α∈ℜ₊, β∈ℜ)
    α = [Convex.Variable(1) for xprime in 1:K]
    constrs +=
        [α[xprime]>=0 for xprime in 1:K]
    β = [Convex.Variable(1) for xprime in 1:K]


    # Dual Cone λ∈Kstar
    λ = [Convex.Variable(N, K) for xprime in 1:K]
    γ = [Convex.Variable(K) for xprime in 1:K]
    Λ = [Convex.Variable(K, K) for xprime in 1:K]
    constrs +=
        [Λ[xprime][x, xp]>=0 for x in 1:K, xp in 1:K, xprime in 1:K]
    constrs += # r=1
        [
            λ[xprime][2, x]>=γ[xprime][x] + sum([Λ[xprime][xp, x]-Λ[xprime][x, xp] for xp in 1:K])
            for x in 1:K, xprime in 1:K
        ] 
    constrs += # r=0
        [λ[xprime][1, x]>=γ[xprime][x] for x in 1:K, xprime in 1:K] 
    constrs +=
        [
            sum(γ[xprime])==L*sum([norm(X[:, x] - X[:, xp])*Λ[xprime][x, xp] for x in 1:K, xp in 1:K])
            for xprime in 1:K
        ]

    # Dual functions
    dualfs = [sum(
        [
            -P[x][r]*relative_entropy(
                η[x], η[x]-λ[xp][r,x]-β[xp] - α[xp]*R[r]*(x==xp))
            for r in 1:N, x in Xt
        ]
    )+sum(
        [
            -P[x][r]*λ[xp][r, x]
            for r in 1:N, x in Xs
        ]
    )+α[xp]*rew_star+β[xp]*length(Xt)
    for xp in 1:K
    ]

    # Implicit Dual function constraints
    constrs +=
        [
            η[x] >= λ[xp][r, x] + β[xp] + α[xp]*R[r]*(x==xp)
            for r in 1:N, x=1:K, xp=1:K
        ]

    # Sufficient exploration
    constrs +=
        [dualfs[xp]>=1 for xp in Xd]

    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
    
    if !isnothing(η.value)
        # Polish solution
        η = [maximum([η.value[x], 0]) for x in 1:K]
        μ = DV(
            [α[xp].value for xp in 1:K],
            [β[xp].value for xp in 1:K],
            [λ[xp].value for xp in 1:K]
        )
    else
        # Fall-Back Option if Mosek can not find solution
        η, μ = deep_target_rates(GenericBandit(B.P, B.R), optimizer=optimizer)
    end
        
    return η, μ
end

"""
    mle(B, N)

    Computes the statistical projection of B onto the Lipschitz bandit class
"""
function mle(B::LipschitzBandit, N_t; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    L = B.L

    K = length(P)
    N = length(R)

    # Best reward
    Xs, rew_star = star(B)
            
    Q = Convex.Variable(N, K)
    m = Convex.Variable(K)

    constrs  =
        [m[x] == sum([Q[r, x]*R[r] for r in 1:N]) for x in 1:K]

    constrs +=
        [
            m[x1]-m[x2]<=L*norm(X[:, x1]-X[:, x2])
            for x1 in 1:K, x2 in 1:K
        ]
    constrs +=
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]

    obj = sum([N_t[x]*relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    problem = minimize(obj, constrs)

    solve!(problem, optimizer; silent_solver=true)
    
    return update(B, [Q.value[:, x] for x in 1:K])
end
