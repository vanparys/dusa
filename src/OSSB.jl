"""
    ossb_round(B::Bandit, N, μ::DV, s, t; ε=0)

    Carries out one round of DuSA algorithm
"""
function ossb_round(B_t::BernoulliLipschitzBandit, N_t, s_t, t; ε=0, verbose=true, optimizer=Mosek.Optimizer)

    if(verbose)
        println("******************")
        println("OSSB round t=", t)
        println("******************")
    end

    # Compute ML estimate (eventually B_mle ≈ B_t)
    B_mle = mle(B_t, N_t, optimizer=optimizer)

    # Bandit
    P_t = B_mle.P
    R = B_mle.R
    
    K = length(B_t.P)
    N = length(R)

    # Partition arms
    Xs, Xd, Xn = DuSA.partition(B_mle, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    
    if(verbose)
        println("Empirical Optimal Arm:", Xs)
        println("Deceitful Arms:", Xd)
        println("Non-Deceitful Arms:", Xn)
    end

    η_t = explicit_target_rates(B_mle)

    s = s_t
    η = η_t
    
    x_t = 1
    if (length(Xt)==0 || minimum((N_t - η_t*log(t)*(1+ε))[Xt]) >= 0)
        # Exploitation        
        x_t = argmax([rew(x, B_mle) for x in 1:K])
        ϕ = "Exploitation"
        if(verbose)
            println("Phase: Exploitation")
            println("Pull arm x_t=", x_t)
        end
    else
        s = s_t + 1
        if(verbose)
            println("Exploration round s_t=", s_t)
            println("Minimum N_t=", minimum(N_t))
            println("Minimum Req.=", ε*s_t)
        end
        if (minimum(N_t)<=ε*s_t)
            # Estimation
            x_t = argmin(N_t)
            ϕ = "Estimation"
            if(verbose)
                println("Phase: Estimation")
                println("Pull arm x_t=", x_t)
            end
        else
            # Exploration
            x_t = argmin(N_t./(η_t*log(t)))
            ϕ = "Exploration"
            if(verbose)
                println("Phase: Exploration")
                println("Targets η_t*log(t)=", η_t*log(t))
                println("N_t=", N_t)
                println("Relative difference N_t/η_t=", N_t./(η_t*log(t)))
                println("Pull arm x_t=", x_t)
            end
        end
    end

    return x_t, s, η, ϕ

end

"""
    ossb_play(B, T)

    Play bandit for T >= K rounds 
"""
function ossb_play(B::BernoulliLipschitzBandit, T)

    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)
    
    # DUSA INITIALIZE
    r = Array{Union{Float64, Missing}}(missing, T)
    x = Array{Union{Int, Missing}}(missing, T)
    N = Array{Union{Array{Union{Int, Missing}}, Missing}}(missing, T+1)
    η = Array{Union{Array{Union{Float64, Missing}}, Missing}}(missing, T+1)
    s = Array{Union{Int, Missing}}(missing, T+1)
    Bₒ= Array{Union{Bandit, Missing}}(missing, T+1)
    Δt = Array{Union{Float64, Missing}}(missing, T)
    ϕ = Array{Union{String, Missing}}(missing, T)
    for t in 1:K   
        x[t] = t
        r[t] = DuSA.pull(x[t], B)
        Δt[t] = 0
        ϕ[t] = "Initialization"
    end    
    Bₒ[K+1] = update(B, [1.0*(B.R.==r[t]) for t=1:K])
    N[K+1]  = ones(K)
    s[K+1]  = 0

    # OSSB MAIN
    for t in K+1:T        
        ts = @timed begin            
            x[t], s[t+1], η[t+1], ϕ[t]  = ossb_round(Bₒ[t], N[t], s[t], t)
        end
        Δt[t] = ts[2]
        
        # PULL
        r[t] = pull(x[t], B)

        # Update P_t
        Pₜ = deepcopy(Bₒ[t].P)
        Pₜ[x[t]] = (Pₜ[x[t]]*N[t][x[t]].+(Bₒ[t].R.==r[t]))/(N[t][x[t]]+1)
        Bₒ[t+1] = update(Bₒ[t], Pₜ)

        # Update N_t
        N[t+1] = deepcopy(N[t])
        N[t+1][x[t]] = N[t][x[t]]+1        
    end
    
    return r, x, N, η, s, Bₒ, Δt, ϕ
end
