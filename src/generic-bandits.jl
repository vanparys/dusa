struct GenericBandit <: Bandit
    P
    R
end

import Printf

function Base.show(io::IO, B::GenericBandit)
    Printf.@printf(io=io, "Generic bandit with K=%d arms and R=%d outcomes", length(B.P), length(B.R))
end

"""
    reward_max(xp, B)
    
    Given a bandit B with reward distribution B.P supported on outcome 
    set B.R with arms B.X compute the maximum conceivable award of arm 
    xp

    @see Function [rew^star(x, P) for x in Xd]
"""
function reward_max(xp, B::GenericBandit; optimizer=Mosek.Optimizer)
    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(P[1])

    # Best arm
    Xs, rew_star = star(B)

    rew_max = 0
    if (xp in Xs)
        rew_max = rew_star
    else
        rew_max = maximum(R)[1]
    end
    
    return rew_max[1]
end

"""
    dist_function_primal(η, xp, B)

    Computes the statistical distance between P and its deceitful 
    distributions using a primal minimization formulation.

    @see Function Dist(η, xp, P)
"""
function dist_function_primal(η, xp, B::GenericBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    # Best reward
    X_star, rew_star = star(B)

    Q = Convex.Variable(N, K)

    constrs  =
        [Q>=0]
    for x_star in X_star
        constrs +=
            [Q[:, x_star]==P[x_star]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [sum([Q[r, xp]*R[r] for r=1:N])>=rew_star]

    obj = sum([
        η[x]*relative_entropy(Constant(P[x]), Q[:, x])
        for x=1:K
    ])
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    return problem.optval, [Q.value[:, x] for x in 1:K], problem.status
end

"""
    dist_function_dual(η, xp, B)

    Computes the statistical distance between P and its deceitful 
    distributions using a dual minimization formulation.
"""
function dist_function_dual(η, xp, B::GenericBandit; optimizer=Mosek.Optimizer)
    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = Convex.Variable(1)
    β = Convex.Variable(1)
    λ = Convex.Variable(N, K)

    # Dual Cone λ∈Kstar
    γ = Convex.Variable(K)
    constrs =
        [λ[r, x]>=γ[x] for r in 1:N, x=1:K]
    constrs += 
        [sum(γ)==0]
    
    # Other Dual constraints
    constrs += 
        [α>=0]
    constrs +=
        [η[x] >= λ[r, x] + β + α*R[r]*(x==xp) for r in 1:N, x=1:K]

    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*relative_entropy(
                Constant(η)[x],η[x]-λ[r, x]-β- α*R[r]*(x==xp)
            )
            for r in 1:N, x in Xt
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+  α*rew_star+β*length(Xt)


    problem = maximize(dualf, constrs)
    solve!(problem, optimizer; silent_solver=true)

    return problem.optval, problem.status
end


"""
    deep_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a generic bandit B.    
"""
function deep_target_rates(B::GenericBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    # Primal variables
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]
    
    obj = sum([η[x]*Δ(x, B) for x in 1:K])

    # Dual variables
    α = [Convex.Variable(1) for xp in 1:K]
    constrs +=
        [α[xp]>=0 for xp in 1:K]
    β = [Convex.Variable(1) for xp in 1:K]
    λ = [Convex.Variable(N, K) for xp in 1:K]

    # Dual Cone λ∈Kstar
    γ = [Convex.Variable(K) for xp in 1:K]
    constrs +=
        [λ[xp][r, x]>=γ[xp][x] for r in 1:N, x in 1:K, xp in 1:K]
    constrs +=
        [sum(γ[xp])==0 for xp in 1:K]

    # Dual functions
    dualfs = [sum(
        [
            -P[x][r]*relative_entropy(
                η[x], η[x]-λ[xp][r,x]-β[xp] - α[xp]*R[r]*(x==xp))
            for r in 1:N, x in Xt
        ]
    )+sum(
        [
            -P[x][r]*λ[xp][r, x]
            for r in 1:N, x in Xs
        ]
    )+α[xp]*rew_star+β[xp]*length(Xt)
    for xp in 1:K
    ]

    
    # Implicit Dual function constraints
    constrs += [
        η[x] >= λ[xp][r, x] + β[xp] + α[xp]*R[r]*(x==xp)
        for r in 1:N, x=1:K, xp=1:K
    ]

    # Sufficient exploration
    if(length(Xd)>=1)
        constrs +=
            [dualfs[xp]>=1 for xp in Xd]
    end
        
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    # Polish solution
    η = [maximum([η.value[x], 0]) for x in 1:K]
    μ = DV(
        [α[xp].value for xp in 1:K],
        [β[xp].value for xp in 1:K],
        [λ[xp].value for xp in 1:K]
    )
    
    return η, μ
end

"""
    test_function(η, xp, B, μ)

    Computes the statistical distance between B.P and its deceitful 
    distributions concerning arm xp using a dual minimization 
    formulation.
"""
function test_function(η, xp, B::GenericBandit, μ::DV; optimizer=Mosek.Optimizer)
    # Bandit
    P = B.P
    R = B.R
    
    K = length(P)
    N = length(R)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = μ.α[xp]
    β = μ.β[xp]
    λ = μ.λ[xp]

    ρ = Convex.Variable(1)
        
    # Other Dual constraints
    constrs  =
        [ρ>=0]
    constrs +=
        [η[x] >= ρ*λ[r, x] + ρ*β + ρ*α*R[r]*(x==xp) for r in 1:N, x=1:K]

    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*relative_entropy(
                Constant(η)[x],η[x]-ρ*λ[r, x]-ρ*β-ρ*α*R[r]*(x==xp)
            )
            for r in 1:N, x in Xt
        ]
    )+  sum(
        [
            -P[x][r]*ρ*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+  ρ*α*rew_star+ρ*β*length(Xt)

    problem = maximize(dualf, constrs)
    solve!(problem, optimizer; silent_solver=true)

    return problem.optval
end

"""
    mle(B, N)

    Computes the statistical projection of B onto the generic bandit class
"""
function mle(B::GenericBandit, N_t; optimizer=Mosek.Optimizer)
    return B
end

"""
    update(B, P_new)

    Return updated bandit
    
"""
function update(B::GenericBandit, P_new)
    return GenericBandit(P_new, B.R)
end
