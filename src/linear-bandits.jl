struct LinearBandit <: Bandit
    P
    R
    X
end

import Printf

function Base.show(io::IO, B::LinearBandit)
    Printf.@printf(io=io, "Linear bandit with K=%d arms embedded in Re^%d and R=%d outcomes", length(B.P), size(B.X, 1), length(B.R))
end

"""
    reward_max(Xp, B)
    
    Given a linear bandit B with reward distribution B.P supported on 
    outcome set B.R with arms B.X compute the maximum conceivable award
    of arms in Xp.

    @see Function [rew^star(x, P) for x in Xd]
"""
function reward_max(xp, B::LinearBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X    
    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Best arm
    Xs, rew_star = star(B)

    # Primal variables
    Q = Convex.Variable(N, K)
    a = Convex.Variable(d)
    m = Convex.Variable(K)

    constrs  =
        [Q>=0]
    for xs in Xs
        constrs +=
            [Q[:, xs]==P[xs]]
    end
    constrs += 
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [sum([Q[r, x]*R[r] for r=1:N])==m[x] for x in 1:K]
    constrs +=
        [m[x]==a'*X[:, x] for x in 1:K]

    problem = maximize(sum([Q[r, xp]*R[r] for r in 1:N]), constrs)
    solve!(problem, optimizer; silent_solver=true)

    return problem.optval
end

"""
    dist_function_primal(η, xp, B)

    Computes the statistical distance between B.P and its deceitful
    distributions concerning arm xp using a primal minimization 
    formulation.

    @see Function Dist(η, xp, P)
"""
function dist_function_primal(η, xp, B::LinearBandit; optimizer=Mosek.Optimizer)
    
    # Bandit
    P = B.P
    R = B.R
    X = B.X
    
    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Best reward
    Xs, rew_star = star(B)
    
    Q = Convex.Variable(N, K)
    a = Convex.Variable(d)
    m = Convex.Variable(K)

    constrs  =
        [Q>=0] 
    for xs in Xs
        constrs +=
            [Q[:, xs]==P[xs]]
    end
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [sum([Q[r, xp]*R[r] for r=1:N])>=rew_star]
    constrs +=
        [sum([Q[r, x]*R[r] for r=1:N])==m[x] for x in 1:K]
    constrs +=
        [m[x]==a'*X[:, x] for x in 1:K]

    obj = sum(
        [η[x]*relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K]
    )
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
        
    return problem.optval, [Q.value[:, x] for x in 1:K], problem.status
end

"""
    dist_function_dual(η, xp, B)

    Computes the statistical distance between B.P and its deceitful 
    distributions concerning arm xp using a dual minimization 
    formulation.

    @see Function max { Dual(η, x, P; μ) : μ ∈ ℜ₊×ℜ×K }
"""
function dist_function_dual(η, xp, B::LinearBandit; optimizer=Mosek.Optimizer)
    # Bandit
    P = B.P
    R = B.R
    X = B.X
    
    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Best reward
    Xs, rew_star = star(B)
    Xt = setdiff(1:K, Xs)
    
    α = Convex.Variable(1)
    β = Convex.Variable(1)
    λ = Convex.Variable(N, K)
    η = Constant(η)
    
    # Dual Cone λ∈Kstar
    γ = Convex.Variable(K)
    ν = Convex.Variable(K)
    constrs  =
        [λ[r, x]>=γ[x]+ν[x]*R[r] for r in 1:N, x=1:K]
    constrs +=
        [sum(γ)==0, sum([X[:, x]*ν[x] for x=1:K])==zeros(d)]
    
    # Other Dual constraints
    constrs +=
        [α>=0]
    constrs +=
        [η[x] >= λ[r, x] + β + α*R[r]*(x==xp) for r in 1:N, x=1:K]

    # Dual function
    dualf =
        sum(
        [
            -P[x][r]*relative_entropy(
                η[x],η[x]-λ[r, x]-β-α*R[r]*(x==xp)
            )
            for r in 1:N, x in Xt
        ]
    )+  sum(
        [
            -P[x][r]*λ[r, x]
            for r in 1:N, x in Xs
        ]
    )+  α*rew_star+β*length(Xt)

    problem = maximize(dualf, constrs)
    solve!(problem, optimizer; silent_solver=true)

    # Return a polished solution
    return problem.optval, DV(maximum([α.value, 0]), β.value, λ.value)
end

"""
    deep_target_rates(B)

    Computes the optimal target rates η with which to pull 
    suboptimal arms assuming a linear bandit B.    

    @see Deep Update Algorithm 
"""
function deep_target_rates(B::LinearBandit; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X

    K = length(P)
    N = length(R)
    d = size(X, 1)

    # Partition
    Xs, Xd, Xn = partition(B, optimizer=optimizer)
    Xt = setdiff(1:K, Xs)
    Xs, rew_star = star(B)

    # Primal variables
    η = Convex.Variable(K)
    constrs  =
        [η[x]>=0 for x in 1:K]
    constrs +=
        [η[x]==0 for x in Xs]

    # Minimize the regret
    obj = sum([η[x]*Δ(x, B) for x in 1:K])

    # Dual variables (α∈ℜ₊, β∈ℜ)
    α = [Convex.Variable(1) for xp in 1:K]
    constrs +=
        [α[xp]>=0 for xp in 1:K]
    β = [Convex.Variable(1) for xp in 1:K]

    # Dual Cone λ∈K^⋆
    λ = [Convex.Variable(N, K) for xp in 1:K]
    γ = [Convex.Variable(K) for xp in 1:K]
    ν = [Convex.Variable(K) for xp in 1:K]

    constrs +=
        [
            λ[xp][r, x]>=γ[xp][x]+ν[xp][x]*R[r]
            for r in 1:N, x in 1:K, xp in 1:K
        ]
    constrs +=
        [sum(γ[xp])==0 for xp in 1:K]
    constrs +=
        [
            sum([X[:, x]*ν[xp][x] for x=1:K])==zeros(d)
            for xp=1:K
        ]

    # Dual functions
    dualfs = [sum(
        [
            -P[x][r]*relative_entropy(
                η[x], η[x]-λ[xp][r,x]-β[xp] - α[xp]*R[r]*(x==xp))
            for r in 1:N, x in Xt
        ]
    )+sum(
        [
            -P[x][r]*λ[xp][r, x]
            for r in 1:N, x in Xs
        ]
    )+α[xp]*rew_star+β[xp]*length(Xt)
    for xp in 1:K
    ]
    
    # Implicit Dual function constraints
    constrs +=
        [
            η[x] >= λ[xp][r,x] + β[xp] + α[xp]*R[r]*(x==xp)
            for r in 1:N, x=1:K, xp=1:K
        ]

    # Sufficient exploration
    if(length(Xd)>=1)
        constrs +=
            [dualfs[xp]>=1 for xp in Xd]
    end

    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    #################
    # Polish solution
    #################
    η = [maximum([η.value[x], 0]) for x in 1:K]
    μ = DV(
        [α[xp].value for xp in 1:K],
        [β[xp].value for xp in 1:K],
        [λ[xp].value for xp in 1:K]
    )
    
    return η, μ
end

"""
    mle(B, N)

    Computes the statistical projection of B onto the linear bandit class
"""
function mle(B::LinearBandit, N_t; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R
    X = B.X
    
    K = length(P)
    N = length(R)
    d = size(X, 1)
    
    Q = Convex.Variable(N, K)
    a = Convex.Variable(d)
    m = Convex.Variable(K)

    constrs  =
        [Q>=0] 
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [sum([Q[r, x]*R[r] for r=1:N])==m[x] for x in 1:K]
    constrs +=
        [m[x]==a'*X[:, x] for x in 1:K]

    obj = sum(
        [N_t[x]*relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K]
    )
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
        
    return LinearBandit([Q.value[:, x] for x in 1:K], B.R, B.X)
end

"""
    update(B, P_new)

    Return updated bandit
    
"""
function update(B::LinearBandit, P_new)
    return LinearBandit(P_new, B.R, B.X)
end
