"""
    klucb(N, x, B::Bandit)

    Return Kullback-Leibler confidence bound on arm x given N observations.
"""
function klucb(N_t, d, x, B::Bandit; optimizer=Mosek.Optimizer)

    P = B.P
    N = length(B.R)

    # Distributional Optimization
    Q = Convex.Variable(N)

    constrs  = [Q>=0, sum(Q)==1]
    constrs += [N_t[x]*relative_entropy(Constant(P[x]), Q)<=d]

    obj = sum([B.R[r]*Q[r] for r=1:N])

    problem = maximize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)
    
    return problem.optval
end

"""
    klucb_round(B::Bandit, N, μ::DV, s, t; ε=1e-3)

    Carries out one round of DuSA algorithm
"""
function klucb_round(B_t::Bandit, N_t, t; optimizer=Mosek.Optimizer, verbose=true)

    if(verbose)
        println("******************")
        println("KL UCB round t=", t)
        println("******************")
    end

    K = length(B_t.P)
    
    ucb = zeros(K)
    Threads.@threads for x in 1:K
        ucb[x] = klucb(N_t, log(t), x, B_t, optimizer=optimizer)
    end

    return argmax([ucb[x] for x in 1:K])
end


"""
    klucb_play(B, T)

    Play bandit for T >= K rounds 
"""
function klucb_play(B::Bandit, T; optimizer=Mosek.Optimizer)

    # Bandit
    P = B.P
    R = B.R

    K = length(P)
    N = length(R)
    
    # DUSA INITIALIZE
    r = Array{Union{Float64, Missing}}(missing, T)
    x = Array{Union{Int, Missing}}(missing, T)
    N = Array{Union{Array{Union{Int, Missing}}, Missing}}(missing, T+1)
    Bₒ= Array{Union{Bandit, Missing}}(missing, T+1)
    Δt = Array{Union{Float64, Missing}}(missing, T)
    for t in 1:K   
        x[t] = t
        r[t] = pull(x[t], B)
        Δt[t] = 0
    end    
    Bₒ[K+1] = update(B, [1.0*(B.R.==r[t]) for t=1:K])
    N[K+1]  = ones(K)

    # KL-UCB MAIN
    for t in K+1:T        
        ts = @timed begin            
            x[t]  = klucb_round(Bₒ[t], N[t], t, optimizer=optimizer)
        end
        Δt[t] = ts[2]
        
        # PULL
        r[t] = pull(x[t], B)

        # Update P_t
        Pₜ = deepcopy(Bₒ[t].P)
        Pₜ[x[t]] = (Pₜ[x[t]]*N[t][x[t]].+(Bₒ[t].R.==r[t]))/(N[t][x[t]]+1)
        Bₒ[t+1] = update(Bₒ[t], Pₜ)

        # Update N_t
        N[t+1] = deepcopy(N[t])
        N[t+1][x[t]] = N[t][x[t]]+1
    end
    
    return r, x, N, Bₒ, Δt
end
