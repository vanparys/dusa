@testset "create-and-update-generic" begin

    N = 5
    K = 10
    R = range(0, 1, length=N)

    # Construction of reward distribution
    P = [rand(N) for x = 1:K]
    P = [P[x]/sum(P[x]) for x =1:K]

    B_generic1 = DuSA.GenericBandit(P, R)
    B_generic2 = DuSA.update(B_generic1, P)

    @test sum([norm(B_generic1.P[x]-B_generic2.P[x]) for x in 1:K])<1e-3
end

