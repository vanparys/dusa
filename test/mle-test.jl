@testset "mle_test" begin

    println("Maximum Likelihood Test")
    
    for bandit in filter(x->occursin(".jld", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")
        K = length(B.P)

        N_t = ones(K)

        # Maximum Likelihood Projection
        Bp = DuSA.mle(B, N_t, optimizer=optimizer)
       
        # Check consistency
        @test maximum([norm(B.P[x]-Bp.P[x]) for x in 1:K])<1e-3

    end

end
