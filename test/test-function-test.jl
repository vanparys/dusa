@testset "test_function_test" begin

    println("Test Function Test")
    
    for bandit in filter(x->occursin(".jld", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")
        K = length(B.P)
        
        Xs, Xd, Xn = DuSA.partition(B, optimizer=optimizer)

        # Compute optimal exploration rates
        η, μ = DuSA.deep_target_rates(B, optimizer=optimizer)

        dual_bar = ones(K)
        Threads.@threads for xp in Xd
            
            dual_bar[xp] = DuSA.test_function(η, xp, B, μ)

        end

        # Check consistency
        @test minimum(dual_bar)>0.94

    end

end
