@testset "target_rates-test" begin

    println("Target Rates Test")
    
    for bandit in filter(x->occursin(".jld", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")
        K = length(B.P)
        
        # Compute optimal exploration rates
        η_du, μstar = DuSA.deep_target_rates(B, optimizer=optimizer)

        # Compute shallow update exploration rates
        η_su, ρ_su = DuSA.shallow_target_rates(B, μstar, optimizer=optimizer)

        # Check consistency
        @test abs(sum([DuSA.Δ(x, B)*(η_du[x]-η_su[x]) for x in 1:K]))<1e-2*sum([DuSA.Δ(x, B)*η_du[x] for x in 1:K])*K+1e-2

    end
end
