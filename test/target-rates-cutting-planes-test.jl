@testset "target_rates_cutting_planes-test" begin

    println("Target Rates Cutting Planes Test")
    
    for bandit in filter(x->occursin(".jld", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")
        K = length(B.P)
        
        # Compute optimal exploration rates
        η_du, μstar = DuSA.deep_target_rates(B, optimizer=optimizer)
        
        # Cutting plane formulation
        η_k, ε = DuSA.cp_target_rates(B, verbose=false, optimizer=optimizer)

        # Check consistency
        @test abs(sum([DuSA.Δ(x, B)*(η_du[x]-η_k[x]) for x in 1:K]))<1e-1*sum([DuSA.Δ(x, B)*η_du[x] for x in 1:K])+0.01

    end
end
