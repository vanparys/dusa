@testset "generic-bandit-primal-distance-test" begin

   for bandit in filter(x->occursin(".jld", x) && occursin("generic", x), readdir("Bandits"))

        B = load("Bandits/"*bandit, "B")
        N = length(B.R)

        # The case N=2 has an analytical solution
        if N==2

            println("Bandit: ", bandit)

            K = length(B.P)
            η = ones(K)
                        
            Xs, Xd, Xn = DuSA.partition(B, optimizer=optimizer)
            Xs, rew_star = DuSA.star(B)

            # Distance
            Da = zeros(K)
            Threads.@threads for xp in Xd
                Da[xp] = DuSA.dist_function_primal(η, xp, B, optimizer=optimizer)[1]          
            end
            
            P_star = vcat([1 1], B.R')\[1; rew_star]
            Db = zeros(K)
            for xp in Xd                
                Db[xp] = η[xp]*DuSA.I(B.P[xp], P_star)            
            end

            @test norm(Db- Da)<1e-4
            
        end
    end
end
