@testset "distance-function-test" begin

    println("Distance Function Test")
    
    for bandit in filter(x->occursin(".jld", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")

        Xs, Xd, Xn = DuSA.partition(B, optimizer=optimizer)

        K = length(B.P)
        η = ones(K)

        d_p = zeros(length(Xd))
        d_d = zeros(length(Xd))

        Threads.@threads for i in 1:length(Xd)
            d_p[i] = DuSA.dist_function_primal(η, Xd[i], B, optimizer=optimizer)[1]          
            d_d[i] = DuSA.dist_function_dual(η, Xd[i], B, optimizer=optimizer)[1]
        end

        for i in 1:length(Xd)
            @test abs(d_p[i]-d_d[i]) <= 1e-3
        end

    end

end
