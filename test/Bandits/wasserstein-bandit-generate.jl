import DuSA
import Random
Random.seed!(324)
using JLD
using Convex
using Mosek

for i in 1:10

    println("Generate instance i: ", i)

    # Bernoilli bandit
    N = 2

    # Random arms
    R = range(0, 1, length=N)
    K = rand(2:10)
    P = [rand(N) for x = 1:K]
    P = [P[x]/sum(P[x]) for x =1:K]
    
    # Random constraints
    Cn = [repeat(1:K, inner=[K]) repeat(1:K, outer=[K])]
    C = size(Cn, 1)
    D = rand(K^2)+0.4*ones(K^2)
    
    # Distributional projection
    Q = Variable(N, K)
    T = [Variable(N, N) for c in 1:C]
    
    constrs =
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [T[c]>=0 for c in 1:C]
    constrs +=
        [
            sum([abs(R[r1]-R[r2])*T[c][r1, r2] for r1 in 1:N, r2 in 1:N]) <= D[c] for c in 1:C
        ]
    for c in 1:C
        constrs +=
            [
                sum([T[c][r1, r2] for r2 in 1:N])==Q[r1, x] for r1 in 1:N, x in Cn[c, 1]
            ]
        constrs +=
            [
                sum([T[c][r1, r2] for r1 in 1:N])==Q[r2, x] for r2 in 1:N, x in Cn[c, 2]
            ]
    end
    
    obj = sum([relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)
    solve!(problem, Mosek.Optimizer; silent_solver=true)

    B = DuSA.WassersteinBandit([Q.value[:, x] for x in 1:K], R, Cn, D)
    
    ηstar, μstar = DuSA.deep_target_rates(B)

    save("wasserstein-bandit-instance-"*string(i)*".jld", "B", B, "ηstar", ηstar)

end
