import DuSA
import Random
Random.seed!(321)
using JLD
using LinearAlgebra
using Convex
using Mosek

""" 
    lipschitz_bandit(K, d, L)

    Construct a L-Lipschitz Bernouilli bandit instance with K arms 
    embedded in R^d 
"""
function lipschitz_bandit(K, d, L; optimizer=Mosek.Optimizer)

    N = 2
    R = range(0, 1, length=N)
    
    # Random arms
    X = randn(d, K)
    X = X./repeat([norm(X[:, k], 2) for k =1:K], 1, d)'
    
    # Construction of reward distribution
    P = [rand(N) for x = 1:K]
    P = [P[x]/sum(P[x]) for x =1:K]

    # Distributional projection
    Q = Variable(N, K)
    m = Variable(K)
    constrs =
        [m[x] == sum([Q[r, x]*R[r] for r in 1:N]) for x in 1:K]
    constrs +=
        [
            m[x1]-m[x2]<=L*norm(X[:, x1]-X[:, x2])
            for x1 in 1:K, x2 in 1:K
        ]
    constrs +=
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    
    obj = sum([relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    P = [Q.value[:, x] for x in 1:K]
    return DuSA.BernoulliLipschitzBandit([P[x]/sum(P[x]) for x in 1:K], R, X, L)
end

# Size of test problem
d = 3

for i in 1:10

    println("Generate instance i: ", i)
    
    K = rand(2:20)
    L = rand(0.1:0.1:5)
    
    # Construct a random linear bandit
    B = lipschitz_bandit(K, d, L)
    
    ηstar, μstar = DuSA.deep_target_rates(B)
    save("bernoulli-lipschitz-bandit-instance-"*string(i)*".jld", "B", B, "ηstar", ηstar)

end
