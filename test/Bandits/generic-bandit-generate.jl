import DuSA
import Random
Random.seed!(321)
using JLD

"""
    generic_bandit(K, N)

    Create a generic bandit instance with K arms each of which
    supports N outcomes.
"""
function generic_bandit(K, N)
    # Random arms
    R = range(0, 1, length=N)

    # Construction of reward distribution
    P = [rand(N) for x = 1:K]
    P = [P[x]/sum(P[x]) for x =1:K]

    return DuSA.GenericBandit(P, R)
end

# Binomial bandits
N = 2

for i in 1:10

    println("Generate instance i: ", i)

    K = rand(2:20)

    # Construct a random linear bandit
    B = generic_bandit(K, N)
    ηstar, μstar = DuSA.deep_target_rates(B)
    save("generic-bandit-instance-"*string(i)*".jld", "B", B, "ηstar", ηstar)

end
