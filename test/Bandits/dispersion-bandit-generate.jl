import DuSA
import Random
Random.seed!(321)
using JLD
using Convex
using Mosek

"""
    dispersion_bandit(K, N)

    Create a generic bandit instance with K arms each of which
    supports N outcomes.
"""
function dispersion_bandit(K, N, γ; optimizer=Mosek.Optimizer)

    # Random arms
    R = range(0, 1, length=N)

    # Construction of reward distribution
    P = [rand(N) for x = 1:K]
    P = [P[x]/sum(P[x]) for x =1:K]

    # Distributional Optimization
    Q = Variable(N, K)
    
    constrs =
        [Q>=0]
    constrs +=
        [sum(Q[:, x])==1 for x in 1:K]
    constrs +=
        [
            sum([R[r]^2*Q[r, x] for r in 1:N]) <= γ[x]*sum([R[r]*Q[r, x] for r in 1:N]) for x in 1:K
        ]
    
    obj = sum([relative_entropy(Constant(P[x]), Q[:, x]) for x=1:K])
    
    problem = minimize(obj, constrs)
    solve!(problem, optimizer; silent_solver=true)

    P = [Q.value[:, x] for x in 1:K]

    return DuSA.DispersionBandit([P[x]/sum(P[x]) for x in 1:K], R, γ)

end

# Non-Binomial bandits
N = 5

for i in 1:10

    println("Generate instance i: ", i)

    K = rand(2:20)
    γ = rand(K)+0.4*ones(K)

    # Construct a random linear bandit
    B = dispersion_bandit(K, N, γ)
    ηstar, μstar = DuSA.deep_target_rates(B)
    save("dispersion-bandit-instance-"*string(i)*".jld", "B", B, "ηstar", ηstar)

end
