@testset "bernoulli-lipschitz-bandit-deep-target-rates-test" begin
    
    for bandit in filter(x->occursin(".jld", x) && occursin("bernoulli-lipschitz", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        Bb = load("Bandits/"*bandit, "B")
        Xs, Xd, Xn = DuSA.partition(Bb, optimizer=optimizer)
        
        Bg = DuSA.GeneralLipschitzBandit(Bb.P, Bb.R, Bb.X, Bb.L)

        ηb, μb = DuSA.deep_target_rates(Bb, optimizer=optimizer)
        ηg, μg = DuSA.deep_target_rates(Bg, optimizer=optimizer)

        # Already tested: @test norm(ηb .- ηg)<=1e-3*K
        @test maximum([abs(DuSA.dual_function(ηb, xp, Bb, μb) - DuSA.dist_function_primal(ηb, xp, Bb, optimizer=optimizer)[1]) for xp in Xd])<1e-2
        
    end
end
