@testset "lipschitz-bandit-reward-max-test" begin
    
    for bandit in filter(x->occursin(".jld", x) && occursin("lipschitz", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")
        K = length(B.P)

        Xs, rew_star = DuSA.star(B)

        # Optimization formula
        rewards_max_o = zeros(K)
        Threads.@threads for x in 1:K
            rewards_max_o[x] = DuSA.reward_max(x, B, optimizer=optimizer)
        end

        # Explicit formula
        rewards_max_a = [minimum([maximum(B.R), minimum([rew_star+B.L*norm(B.X[:, xp] - B.X[:, xs]) for xs in Xs ])]) for xp in 1:K]
        
        # Check consistency
        @test norm(rewards_max_o- rewards_max_a)<1e-4

    end
end

@testset "lipschitz-bandit-deep-update-test" begin
    
    for bandit in filter(x->occursin(".jld", x) && occursin("bernoulli-lipschitz", x), readdir("Bandits"))

        println("Bandit: ", bandit)
        B = load("Bandits/"*bandit, "B")
        ηstar = load("Bandits/"*bandit, "ηstar")
        
        Xs, Xd, Xn = DuSA.partition(B, optimizer=optimizer)
        Xs, rew_star = DuSA.star(B)

        N = length(B.R) 
        K = length(B.P)        

        η = DuSA.explicit_target_rates(B, optimizer=optimizer)
        
        # Check consistency
        @test sum([(ηstar[x]-η[x])*DuSA.Δ(x, B) for x in 1:K])/(sum([ηstar[x]*DuSA.Δ(x, B) for x in 1:K])*K)<1e-2

    end
end
