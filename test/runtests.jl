import DuSA
using Test
using LinearAlgebra
import Random
using JLD
using Mosek
using MosekTools
using Convex

Random.seed!(1237)

using Logging
io = open("log-tests.txt", "w+")
IOStream("log-tests.txt")
logger = SimpleLogger(io)
global_logger(logger)

######
optimizer=Mosek.Optimizer
######

include("create-update-bandit-test.jl")

# Lipschitz Bandit

include("lipschitz-bandit-test.jl")
include("bernoulli-lipschitz-bandit-test.jl")

# Generic Bandit

include("generic-bandit-test.jl")

# Generic Tests
include("mle-test.jl")
include("distance-function-test.jl")
include("target-rates-test.jl")
include("target-rates-cutting-planes-test.jl")
include("test-function-test.jl")

close(io)
